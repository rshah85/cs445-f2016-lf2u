=================================================== CS445 ====================================================

=================================================== README ===================================================

Name : Rahi Dharmendra Shah

CWID : A20343885

Email : rshah85@hawk.iit.edu

Contact No : 630-814-9322

Subject : Project

===============================================================================================================

1) Configuraion Instructions

-- Download and Install jdk
-- Check the JAVA_HOME if not set then use the following commnad to set (export JAVA_HOME=/path/to/jdk/)
-- Download and Install apache-ant-1.9.7
-- Check the ANT_HOME if not set then use the following command to set (export ANT_HOME=/path/to/ant)
-- Install git using this command (sudo apt-get install git)
-- Download and Install apache-tomcat-8.0.33

===============================================================================================================

2) Build and Deploy Instaruction 

Build and Deploy Source code:
--------------------------------------------------------------------------
-- I have provided a script named as script_1_Install_lf2u.sh
-- This script is consist of many steps.
-- (Step - 1) It will download and install all the required software packages, 
   setting up the environment variables, then cloning the repository.
-- (Step - 2) This step will create executable .war file inside the project directory
-- (Step - 3) This step will copy the .war file into webapps directory of apache-tomcat-8.0.33
-- (Step - 4) Start the server.

Build and Generate junit coverage report
---------------------------------------------------------------------------
-- I have provided a script named as script_2_Generate_junit_coverage_report.sh
-- Run this script.
-- Go to project directory and then us the command (cd junit/CoverageReports)
-- The script has created index.html
-- Open this file in any Browser and You can see the junit report.

===============================================================================================================

3) Known Bugs

-- I have developed unit test for almost each and every usecase to test them whether they are passing or not.
   And I ran the cycle couple of times which consist of running the project, running the unit test, find bugs 
   and fix them. After couple of cycles I can able to reach at the place where my project can be bugs free.
-- I don't have any known bug.

===============================================================================================================

4) Credits and acknowledgements

-- In order to successful completion of this project I would like to give credit to the following persons for there
   continuous support and guidance.
   1) Professor Virgil Bistriceanu
   2) Alexandru Orhean