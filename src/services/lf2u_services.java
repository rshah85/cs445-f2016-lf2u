package services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import POJO.CIDPojo;
import POJO.Catalog;
import POJO.Consumer;
import POJO.Delivery_Charge;
import POJO.FSPAmount;
import POJO.FSPIDPojo;
import POJO.FamerReportID;
import POJO.FarmWithID;
import POJO.Farmer;
import POJO.FarmerOrders;
import POJO.FarmerReport;
import POJO.FarmerWithDeliveryCharge;
import POJO.GCPIDPojo;
import POJO.IDPojo;
import POJO.ManagerAccount;
import POJO.ManagerReport;
import POJO.OIDPojo;
import POJO.Order;
import POJO.OrderByID;
import POJO.OrderDetail;
import POJO.Ordered_By;
import POJO.Product;
import POJO.ProductPrice;
import POJO.ProductWithFSP;
import POJO.Report_Yesterday;
import POJO.RevenueReport;
import POJO.farm_info;
import POJO.personal_info;
import edu.iit.cs445.f2016.lf2u.BoundaryInterface;
import edu.iit.cs445.f2016.lf2u.NullConsumer;
import edu.iit.cs445.f2016.lf2u.NullFarmer;
import edu.iit.cs445.f2016.lf2u.NullManagerAccount;
import edu.iit.cs445.f2016.lf2u.UniqueIdGenerator;

public class lf2u_services implements BoundaryInterface {
	public static List<Farmer> farmer = new ArrayList<>();
	public static List<FarmWithID> farmerwithId = new ArrayList<>();
	public static List<Consumer> consumer = new ArrayList<>();
	public static List<Order> order = new ArrayList<>();
	public static List<OrderDetail> order_detail = new ArrayList<>();
	public static List<Product> product = new ArrayList<>();
	public static List<ProductWithFSP> productFSPList = new ArrayList<>();
	public static List<Catalog> catalog = new ArrayList<>();
	public static List<ManagerAccount> manager = new ArrayList<>();
	public static List<FarmerReport> report = new ArrayList<>();
	public static List<FSPAmount> fsporderdetail = new ArrayList<>();
	public static List<FarmerWithDeliveryCharge> farmerdelivery = new ArrayList<>();
	public static List<ManagerReport> manager_report = new ArrayList<>();

	// Create farmer account
	public IDPojo createAccount(Farmer far, int pinsize) {
		FarmWithID fnid = new FarmWithID();
		farm_info fi = new farm_info();
		ArrayList<String> list = new ArrayList<String>();
		fi.setName(far.getFarm_info().getName());
		fi.setAddress(far.getFarm_info().getAddress());
		fi.setPhone(far.getFarm_info().getPhone());
		fi.setWeb(far.getFarm_info().getWeb());
		personal_info pi = new personal_info();
		pi.setName(far.getPersonal_info().getName());
		pi.setEmail(far.getPersonal_info().getEmail());
		pi.setPhone(far.getPersonal_info().getPhone());
		for (int i = 0; i < pinsize; i++) {
			list.add(far.getDelivers_to().get(i));
		}
		far.setFarm_info(fi);
		far.setPersonal_info(pi);
		far.setDelivers_to(list);
		int fid = UniqueIdGenerator.getUniqueID();
		fnid.setFarmer(far);
		fnid.setFid(String.valueOf(fid));
		farmerwithId.add(fnid);
		farmer.add(far);
		System.out.println("Unique id for farmer is ---:: " + fid);
		IDPojo id = new IDPojo();
		id.setId(String.valueOf(fid));
		if (report.size() == 0) {
			FarmerReport fr = new FarmerReport();
			fr.setFrid("1");
			fr.setName("Orders to deliver today");
			report.add(fr);
			FarmerReport fr1 = new FarmerReport();
			fr1.setFrid("2");
			fr1.setName("Orders to deliver tomorrow");
			report.add(fr1);
			FarmerReport fr2 = new FarmerReport();
			fr2.setFrid("3");
			fr2.setName("Revenue report");
			report.add(fr2);
			FarmerReport fr3 = new FarmerReport();
			fr3.setFrid("4");
			fr3.setName("Orders delivery report");
			report.add(fr3);
		}
		return id;
	}

	// Update farmer account
	public IDPojo updateAccount(Farmer far, int fid, int pinsize) {
		FarmWithID fnid = new FarmWithID();
		farm_info fi = new farm_info();
		ArrayList<String> list = new ArrayList<String>();
		System.out.println("Farm name -- " + far.getFarm_info().getName());
		fi.setName(far.getFarm_info().getName());
		fi.setAddress(far.getFarm_info().getAddress());
		fi.setPhone(far.getFarm_info().getPhone());
		fi.setWeb(far.getFarm_info().getWeb());
		personal_info pi = new personal_info();
		pi.setName(far.getPersonal_info().getName());
		pi.setEmail(far.getPersonal_info().getEmail());
		pi.setPhone(far.getPersonal_info().getPhone());
		for (int i = 0; i < pinsize; i++) {
			list.add(far.getDelivers_to().get(i));
		}
		far.setFarm_info(fi);
		far.setPersonal_info(pi);
		far.setDelivers_to(list);
		fid = fid - 1;
		fnid.setFarmer(far);
		fnid.setFid((String.valueOf(fid + 1)));
		farmerwithId.set(fid, fnid);
		farmer.set(fid, far);
		fid = fid + 1;
		System.out.println("Unique id for farmer is ---:: " + fid);
		IDPojo id = new IDPojo();
		id.setId(String.valueOf(fid));
		return id;
	}

	public List<Farmer> viewAllFarmers() {
		return (farmer);
	}

	public Farmer viewFarmer(int id) {
		Iterator<FarmWithID> or = farmerwithId.listIterator();
		while (or.hasNext()) {
			FarmWithID o = or.next();
			if (o.matchesFid(String.valueOf(id))) {
				Farmer fr = new Farmer();
				fr.setDelivers_to(o.getFarmer().getDelivers_to());
				fr.setFarm_info(o.getFarmer().getFarm_info());
				fr.setPersonal_info(o.getFarmer().getPersonal_info());
				return fr;
			}
		}
		return (new NullFarmer());
	}

	// view farmer that deliver in specific zipcode
	public List<Farmer> viewFarmerByZipcode(String zipcode) {
		List<Farmer> farmerByZip = new ArrayList<>();
		Iterator<Farmer> or = farmer.listIterator();
		while (or.hasNext()) {
			Farmer o = or.next();
			if (o.matchesByZip(zipcode)) {
				farmerByZip.add(o);
			}
		}
		return (farmerByZip);
	}

	// Create product
	public FSPIDPojo addProductFromCatalog(Product pr, int fid) {
		pr.setFid(String.valueOf(fid));
		int fspid = UniqueIdGenerator.getProductID();
		pr.setFspid(String.valueOf(fspid));
		String prodname = null;
		Iterator<Catalog> ct = lf2u_services.catalog.listIterator();
		while (ct.hasNext()) {
			Catalog c = ct.next();
			if (c.matchesGcpid(pr.getGcpid())) {
				prodname = c.getName();
			}
		}
		pr.setName(prodname);
		product.add(pr);
		System.out.println("Product Unit is----" + pr.getProduct_unit());
		System.out.println("Id for product is ---:: " + fspid);
		System.out.println("Product name is ----- " + pr.getName());

		FSPIDPojo id = new FSPIDPojo();
		id.setFspid(String.valueOf(fspid));
		ProductWithFSP pfsp = new ProductWithFSP();
		pfsp.setName(pr.getName());
		pfsp.setEnd_date(pr.getEnd_date());
		pfsp.setImage(pr.getImage());
		pfsp.setNote(pr.getNote());
		pfsp.setPrice(pr.getPrice());
		pfsp.setProduct_unit(pr.getProduct_unit());
		pfsp.setStart_date(pr.getStart_date());
		pfsp.setFspid(id.getFspid());
		productFSPList.add(pfsp);
		return id;
	}

	public FSPIDPojo updateProductFromCatalog(Product pr, int fid, int fspid) {
		System.out.println("Id for product 1 is ---:: " + fspid);
		fspid = fspid - 1;
		Product prd = new Product();
		if (pr.getName() == null) {
			prd.setName(product.get(fspid).getName());
		} else {
			prd.setName(pr.getName());
		}
		if (pr.getEnd_date() == null) {
			prd.setEnd_date(product.get(fspid).getEnd_date());
		} else {
			prd.setEnd_date(pr.getEnd_date());
		}
		if (pr.getFid() == null) {
			prd.setFid(product.get(fspid).getFid());
		} else {
			prd.setFid(pr.getFid());
		}
		prd.setFspid(String.valueOf(fspid + 1));
		if (pr.getGcpid() == null) {
			prd.setGcpid(product.get(fspid).getGcpid());
		} else {
			prd.setGcpid(pr.getGcpid());
		}
		if (pr.getImage() == null) {
			prd.setImage(product.get(fspid).getImage());
		} else {
			prd.setImage(pr.getImage());
		}
		if (pr.getNote() == null) {
			prd.setNote(product.get(fspid).getNote());
		} else {
			prd.setNote(pr.getNote());
		}
		if (pr.getPrice() == 0.0) {
			prd.setPrice(product.get(fspid).getPrice());
		} else {
			prd.setPrice(pr.getPrice());
		}
		if (pr.getProduct_unit() == null) {
			prd.setProduct_unit(product.get(fspid).getProduct_unit());
		} else {
			prd.setProduct_unit(pr.getProduct_unit());
		}
		if (pr.getStart_date() == null) {
			prd.setStart_date(product.get(fspid).getStart_date());
		} else {
			prd.setStart_date(pr.getStart_date());
		}
		product.set(fspid, prd);
		fspid = fspid + 1;
		FSPIDPojo id = new FSPIDPojo();
		id.setFspid(String.valueOf(fspid));
		ProductWithFSP pfsp = new ProductWithFSP();
		pfsp.setName(prd.getName());
		pfsp.setEnd_date(prd.getEnd_date());
		pfsp.setImage(prd.getImage());
		pfsp.setNote(prd.getNote());
		pfsp.setPrice(prd.getPrice());
		pfsp.setProduct_unit(prd.getProduct_unit());
		pfsp.setFspid(String.valueOf(fspid));
		pfsp.setStart_date(prd.getStart_date());
		pfsp.setFspid(id.getFspid());
		fspid = fspid - 1;
		productFSPList.set(fspid, pfsp);
		fspid = fspid + 1;
		System.out.println("Product Unit is----" + prd.getProduct_unit());
		System.out.println("Product Price is----" + prd.getPrice());
		System.out.println("Id for product is ---:: " + fspid);
		return id;
	}

	public List<ProductWithFSP> viewAllProductInStore(int fid) {
		List<ProductWithFSP> pfsp = new ArrayList<>();
		Iterator<Product> or = product.listIterator();
		while (or.hasNext()) {
			Product o = or.next();
			if (o.matchesfid(String.valueOf(fid))) {
				ProductWithFSP pf = new ProductWithFSP();
				pf.setName(o.getName());
				pf.setEnd_date(o.getEnd_date());
				pf.setImage(o.getImage());
				pf.setNote(o.getNote());
				pf.setPrice(o.getPrice());
				pf.setProduct_unit(o.getProduct_unit());
				pf.setStart_date(o.getStart_date());
				pf.setFspid(o.getFspid());
				pfsp.add(pf);
			}
		}
		return pfsp;
	}

	public List<FarmerReport> viewFarmerReport(int fid) {
		if (report.size() == 0) {
			FarmerReport fr = new FarmerReport();
			fr.setFrid("1");
			fr.setName("Orders to deliver today");
			report.add(fr);
			FarmerReport fr1 = new FarmerReport();
			fr1.setFrid("2");
			fr1.setName("Orders to deliver tomorrow");
			report.add(fr1);
			FarmerReport fr2 = new FarmerReport();
			fr2.setFrid("3");
			fr2.setName("Revenue report");
			report.add(fr2);
			FarmerReport fr3 = new FarmerReport();
			fr3.setFrid("4");
			fr3.setName("Orders delivery report");
			report.add(fr3);
		}
		return report;
	}

	// TODO - Add amount
	public FamerReportID viewFarmerReportById(int fid, int frid) {
		System.out.println("FRID---" + frid);
		FamerReportID reportid = new FamerReportID();
		if (frid == 1) {
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			Date d = new java.util.Date();
			String todayDate = df.format(d);
			reportid.setFrid(String.valueOf(frid));
			Iterator<FarmerReport> fr = report.listIterator();
			while (fr.hasNext()) {
				FarmerReport f = fr.next();
				if (f.matchesFrid(String.valueOf(frid))) {
					reportid.setName(f.getName());
				}
			}
			List<FarmerOrders> orderList = new ArrayList<>();
			String cid = null;
			Iterator<Order> or = order.listIterator();
			while (or.hasNext()) {
				Order o = or.next();
				if (o.matchesFid(String.valueOf(fid))) {
					if (o.getPlanned_delivery_date().equals(todayDate)) {
						List<POJO.order_detail> odList = new ArrayList<>();
						FarmerOrders fo = new FarmerOrders();
						fo.setActual_delivery_date(o.getActual_delivery_date());
						fo.setNote(o.getDelivery_note());
						fo.setOid(o.getOid());
						fo.setOrder_date(o.getOrder_date());
						fo.setPlanned_delivery_date(o.getPlanned_delivery_date());
						fo.setStatus(o.getStatus());

						double product_total = 0.0;
						Iterator<FSPAmount> fsp = fsporderdetail.listIterator();
						while (fsp.hasNext()) {
							FSPAmount f = fsp.next();
							String name = null;
							if (f.matchesOid(o.getOid())) {
								Iterator<ProductWithFSP> pfsp = productFSPList.listIterator();
								while (pfsp.hasNext()) {
									ProductWithFSP pf = pfsp.next();
									if (pf.matchesFspid(f.getFspid())) {
										name = pf.getName();
									}
								}
								System.out.println("Name is-----------" + name);
								POJO.order_detail od = new POJO.order_detail();
								od.setFspid(f.getFspid());
								od.setName(name);
								od.setOrder_size(String.valueOf(f.getAmount()));
								od.setPrice(f.getPrice());
								od.setLine_item_total(f.getLine_amount_total());
								odList.add(od);
								product_total += f.getLine_amount_total();
							}
						}
						fo.setProduct_total(product_total);

						fo.setOrder_detail(odList);
						cid = o.getCid();

						System.out.println("Customer id is -->> " + cid);
						Iterator<Consumer> cr = consumer.listIterator();
						while (cr.hasNext()) {
							Consumer c = cr.next();
							if (c.matchesCid(cid)) {
								Ordered_By ob = new Ordered_By();
								ob.setEmail(c.getEmail());
								ob.setName(c.getName());
								ob.setPhone(c.getPhone());
								fo.setOrderby(ob);
								String delivery_address = c.getStreet() + " , " + c.getZip();
								fo.setDelivery_address(delivery_address);
							}
						}
						double dc = 0.0;
						Iterator<FarmerWithDeliveryCharge> fdc = farmerdelivery.listIterator();
						while (fdc.hasNext()) {
							FarmerWithDeliveryCharge f = fdc.next();
							if (f.matchesFid(String.valueOf(fid))) {
								dc = f.getDelivery_charge();
								fo.setDelivery_charge(dc);
							}
						}
						double total = product_total + dc;
						fo.setOrder_total(total);
						orderList.add(fo);
					}
				}
			}
			reportid.setOrders(orderList);
		} else if (frid == 2) {
			String today = null; // Start date
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			Date d = new java.util.Date();
			today = sdf.format(d);
			Calendar cd = Calendar.getInstance();
			try {
				cd.setTime(sdf.parse(today));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			cd.add(Calendar.DATE, 1); // number of days to add
			today = sdf.format(cd.getTime());
			reportid.setFrid(String.valueOf(frid));
			Iterator<FarmerReport> fr = report.listIterator();
			while (fr.hasNext()) {
				FarmerReport f = fr.next();
				if (f.matchesFrid(String.valueOf(frid))) {
					reportid.setName(f.getName());
				}
			}
			List<FarmerOrders> orderList = new ArrayList<>();
			String cid = null;
			Iterator<Order> or = order.listIterator();
			while (or.hasNext()) {
				Order o = or.next();
				if (o.matchesFid(String.valueOf(fid))) {
					if (o.getPlanned_delivery_date().equals(today)) {
						List<POJO.order_detail> odList = new ArrayList<>();
						FarmerOrders fo = new FarmerOrders();
						fo.setActual_delivery_date(o.getActual_delivery_date());
						fo.setNote(o.getDelivery_note());
						fo.setOid(o.getOid());
						fo.setOrder_date(o.getOrder_date());
						fo.setPlanned_delivery_date(o.getPlanned_delivery_date());
						fo.setStatus(o.getStatus());

						double product_total = 0.0;
						Iterator<FSPAmount> fsp = fsporderdetail.listIterator();
						while (fsp.hasNext()) {
							FSPAmount f = fsp.next();
							String name = null;
							if (f.matchesOid(o.getOid())) {
								Iterator<ProductWithFSP> pfsp = productFSPList.listIterator();
								while (pfsp.hasNext()) {
									ProductWithFSP pf = pfsp.next();
									if (pf.matchesFspid(f.getFspid())) {
										name = pf.getName();
									}
								}
								System.out.println("Name is-----------" + name);
								POJO.order_detail od = new POJO.order_detail();
								od.setFspid(f.getFspid());
								od.setName(name);
								od.setOrder_size(String.valueOf(f.getAmount()));
								od.setPrice(f.getPrice());
								od.setLine_item_total(f.getLine_amount_total());
								odList.add(od);
								product_total += f.getLine_amount_total();
							}
						}
						fo.setProduct_total(product_total);

						fo.setOrder_detail(odList);
						cid = o.getCid();

						System.out.println("Customer id is -->> " + cid);
						Iterator<Consumer> cr = consumer.listIterator();
						while (cr.hasNext()) {
							Consumer c = cr.next();
							if (c.matchesCid(cid)) {
								Ordered_By ob = new Ordered_By();
								ob.setEmail(c.getEmail());
								ob.setName(c.getName());
								ob.setPhone(c.getPhone());
								fo.setOrderby(ob);
								String delivery_address = c.getStreet() + " , " + c.getZip();
								fo.setDelivery_address(delivery_address);
							}
						}
						double dc = 0.0;
						Iterator<FarmerWithDeliveryCharge> fdc = farmerdelivery.listIterator();
						while (fdc.hasNext()) {
							FarmerWithDeliveryCharge f = fdc.next();
							if (f.matchesFid(String.valueOf(fid))) {
								dc = f.getDelivery_charge();
								fo.setDelivery_charge(dc);
							}
						}
						double total = product_total + dc;
						fo.setOrder_total(total);
						orderList.add(fo);
					}
				}
			}
			reportid.setOrders(orderList);
		}
		return reportid;
	}

	// TODO - Add Product Revenue....
	public RevenueReport viewRevenueReport(int fid, int frid, String startdate, String enddate) {
		RevenueReport rreport = new RevenueReport();
		rreport.setFrid(String.valueOf(frid));
		Iterator<FarmerReport> fr = report.listIterator();
		while (fr.hasNext()) {
			FarmerReport f = fr.next();
			if (f.matchesFrid(String.valueOf(frid))) {
				rreport.setName(f.getName());
			}
		}
		rreport.setStart_date(startdate);
		rreport.setEnd_date(enddate);

		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Date End_date = new Date();
		Calendar calendar = new GregorianCalendar();
		try {
			calendar.setTime(df.parse(enddate));

			calendar.add(Calendar.DATE, 1);
			End_date = calendar.getTime();

			Date Start_date = new Date();
			calendar.setTime(df.parse(startdate));

			calendar.add(Calendar.DATE, -1);
			Start_date = calendar.getTime();
			int orderplaced = 0;
			int ordercancelled = 0;
			int orderdelivered = 0;
			double delivery_revenue = 0.0;
			double product_total = 0.0;
			System.out.println("Order size --- " + order.size());
			for (int i = 0; i < order.size(); i++) {
				String orderDate = order.get(i).getOrder_date();
				calendar.setTime(df.parse(orderDate));
				Date Order_Date = calendar.getTime();

				if (Order_Date.after(Start_date) && Order_Date.before(End_date)) {
					orderplaced++;
					if (order.get(i).getStatus().equals("cancelled")) {
						ordercancelled++;
					}
					if (order.get(i).getStatus().equals("delivered")) {
						orderdelivered++;
					}
					String id = order.get(i).getFid();
					Iterator<FarmerWithDeliveryCharge> ff = farmerdelivery.listIterator();
					while (ff.hasNext()) {
						FarmerWithDeliveryCharge fdc = ff.next();
						if (fdc.matchesFid(id)) {
							delivery_revenue += fdc.getDelivery_charge();
						}
					}
					String match = order.get(i).getOid();

					Iterator<FSPAmount> fsp = fsporderdetail.listIterator();
					while (fsp.hasNext()) {
						FSPAmount f = fsp.next();
						String name = null;
						if (f.matchesOid(match)) {
							product_total += f.getLine_amount_total();
						}
					}
				}
			}
			rreport.setProducts_revenue(product_total);
			rreport.setDelivery_revenue(delivery_revenue);
			rreport.setOrders_placed(orderplaced);
			rreport.setOrders_cancelled(ordercancelled);
			rreport.setOrders_delivered(orderdelivered);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rreport;
	}

	public void createDeliveryCharge(Delivery_Charge dc, int fid) {
		FarmerWithDeliveryCharge fdc = new FarmerWithDeliveryCharge();
		fdc.setDelivery_charge(dc.getDelivery_charge());
		fdc.setFid(String.valueOf(fid));
		farmerdelivery.add(fdc);
	}

	public Delivery_Charge viewCharge(int fid) {
		Delivery_Charge dc = new Delivery_Charge();
		Iterator<FarmerWithDeliveryCharge> ff = farmerdelivery.listIterator();
		System.out.println("Size -- " + farmerdelivery.size());
		while (ff.hasNext()) {
			FarmerWithDeliveryCharge fd = ff.next();
			if (fd.matchesFid(String.valueOf(fid))) {
				dc.setDelivery_charge(fd.getDelivery_charge());
			}
		}
		return dc;
	}

	// Create Customer account
	public CIDPojo createCustomerAccount(Consumer cr) {
		int cid = UniqueIdGenerator.getConsumerID();
		System.out.println("Unique id for Consumer is ---:: " + cid);
		cr.setCid(String.valueOf(cid));
		consumer.add(cr);
		CIDPojo id = new CIDPojo();
		id.setCid(String.valueOf(cid));
		return id;
	}

	// Update farmer account
	public CIDPojo updateCustomerAccount(Consumer cr, int cid) {
		cr.setCid(String.valueOf(cid));
		cid = cid - 1;
		consumer.set(cid, cr);
		cid = cid + 1;
		System.out.println("Unique id for Consumer is ---:: " + cid);
		CIDPojo id = new CIDPojo();
		id.setCid(String.valueOf(cid));
		return id;
	}

	public Consumer viewCustomer(int cid) {
		Iterator<Consumer> cr = consumer.listIterator();
		Consumer o = null;
		while (cr.hasNext()) {
			o = cr.next();
			if (o.matchesCid(String.valueOf(cid))) {
				return o;
			}
		}
		return (new NullConsumer());
	}

	public OIDPojo createOrder(Order or, int cid) {
		int oid = UniqueIdGenerator.getOrderID();
		System.out.println("Unique id for Order is ---:: " + oid);
		or.setOid(String.valueOf(oid));
		or.setCid(String.valueOf(cid));
		or.setActual_delivery_date("");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar c = Calendar.getInstance();
		c.setTime(new java.util.Date());
		c.add(Calendar.DATE, 1);
		or.setOrder_date(sdf.format(new java.util.Date()));
		or.setPlanned_delivery_date(sdf.format(c.getTime()));
		or.setStatus("open");
		System.out.println("Order_detail size---- " + or.getOrder_detail().size());
		for (int i = 0; i < or.getOrder_detail().size(); i++) {
			FSPAmount fspamt = new FSPAmount();
			fspamt.setAmount(or.getOrder_detail().get(i).getAmount());
			fspamt.setFspid(or.getOrder_detail().get(i).getFspid());
			fspamt.setOid(String.valueOf(oid));
			fspamt.setName(or.getOrder_detail().get(i).getName());
			double amt = or.getOrder_detail().get(i).getAmount();
			double price = 0.0;
			Iterator<ProductWithFSP> pp = productFSPList.listIterator();
			while (pp.hasNext()) {
				ProductWithFSP p = pp.next();
				if (p.matchesFspid(or.getOrder_detail().get(i).getFspid())) {
					price = p.getPrice();

				}
			}
			double line_item = (amt * price);
			fspamt.setPrice(String.valueOf(price));
			fspamt.setLine_amount_total(line_item);
			fsporderdetail.add(fspamt);
		}
		order.add(or);
		OIDPojo id = new OIDPojo();
		id.setOid(String.valueOf(oid));
		return id;
	}

	public List<OrderDetail> viewOrder(int cid) {
		List<OrderDetail> odList = new ArrayList<>();
		Iterator<Order> cr = order.listIterator();
		while (cr.hasNext()) {
			Order o = cr.next();
			if (o.matchesCid(String.valueOf(cid))) {
				OrderDetail od = new OrderDetail();
				od.setActual_delivery_date(o.getActual_delivery_date());
				od.setFid(o.getFid());
				od.setOid(o.getOid());
				od.setOrder_date(o.getOrder_date());
				od.setPlanned_delivery_date(o.getPlanned_delivery_date());
				System.out.println("status  " + o.getStatus() + "  oid  " + o.getOid());
				od.setStatus(o.getStatus());
				odList.add(od);
			}
		}
		return odList;
	}

	public OrderByID viewOrderById(int oid) {
		OrderByID orderby = new OrderByID();
		Iterator<Order> or = order.listIterator();
		String fid = null;
		while (or.hasNext()) {
			Order o = or.next();
			if (o.matchesOid(String.valueOf(oid))) {
				orderby.setOid(String.valueOf(oid));
				orderby.setOrder_date(o.getOrder_date());
				orderby.setPlanned_delivery_date(o.getPlanned_delivery_date());
				orderby.setActual_delivery_date(o.getActual_delivery_date());
				orderby.setStatus(o.getStatus());
				orderby.setDelivery_note(o.getDelivery_note());
				fid = o.getFid();
			}
		}

		Iterator<FarmWithID> fr = farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(fid)) {
				Farmer ff = f.getFarmer();
				farm_info fi = new farm_info();
				fi.setFid(f.getFid());
				fi.setAddress(ff.getFarm_info().getAddress());
				fi.setName(ff.getFarm_info().getName());
				fi.setPhone(ff.getFarm_info().getPhone());
				fi.setWeb(ff.getFarm_info().getWeb());
				orderby.setFarminfo(fi);
			}
		}
		double product_total = 0.0;
		List<POJO.order_detail> orderlist = new ArrayList<>();
		Iterator<FSPAmount> od = fsporderdetail.listIterator();
		while (od.hasNext()) {
			FSPAmount fa = od.next();
			if (fa.matchesOid(String.valueOf(oid))) {
				POJO.order_detail ood = new POJO.order_detail();
				ood.setFspid(fa.getFspid());
				String name = null;
				Iterator<ProductWithFSP> pfsp = productFSPList.listIterator();
                                        while (pfsp.hasNext()) {
                                                ProductWithFSP pf = pfsp.next();
                                                if (pf.matchesFspid(fa.getFspid())) {
                                        		name = pf.getName();
                                		}
                                	}
				ood.setName(name);
				ood.setAmount(String.valueOf(fa.getAmount()));
				ood.setPrice(fa.getPrice());
				ood.setLine_item_total(fa.getLine_amount_total());
				product_total += fa.getLine_amount_total();
				orderlist.add(ood);
			}
		}
		orderby.setOrderdetail(orderlist);
		orderby.setProducts_total(product_total);
		Iterator<FarmerWithDeliveryCharge> fdc = farmerdelivery.listIterator();
		while (fdc.hasNext()) {
			FarmerWithDeliveryCharge f = fdc.next();
			if (f.matchesFid(fid)) {
				orderby.setDelivery_charge(f.getDelivery_charge());
			}
		}
		double total = product_total + orderby.getDelivery_charge();
		orderby.setOrder_total(total);
		return orderby;
	}

	public void cancelOrder(int oid) {
		Iterator<Order> oo = order.listIterator();
		oid = oid - 1;
		System.out.println("oid for list is--- " + oid);
		while (oo.hasNext()) {
			Order o = oo.next();
			if (o.matchesOid(String.valueOf(oid + 1))) {
				o.setActual_delivery_date(o.getActual_delivery_date());
				o.setCid(o.getCid());
				o.setDelivery_note(o.getDelivery_note());
				o.setFid(o.getFid());
				o.setOid(String.valueOf(oid + 1));
				o.setOrder_date(o.getOrder_date());
				o.setOrder_detail(o.getOrder_detail());
				o.setPlanned_delivery_date(o.getPlanned_delivery_date());
				o.setStatus("cancelled");
				order.set(oid, o);
			}
		}
		oid = oid + 1;
	}

	public List<Farmer> searchFarmer(String key) {
		List<Farmer> farmerList = new ArrayList<>();
		System.out.println("In....searchFarmer" + key);
		for (int i = 0; i < farmer.size(); i++) {
			if (farmer.get(i).getFarm_info().getAddress().toLowerCase().contains(key.toLowerCase())
					|| farmer.get(i).getFarm_info().getName().toLowerCase().contains(key.toLowerCase())
					|| farmer.get(i).getFarm_info().getPhone().toLowerCase().contains(key.toLowerCase())
					|| farmer.get(i).getFarm_info().getWeb().toLowerCase().contains(key.toLowerCase())
					|| farmer.get(i).getPersonal_info().getEmail().toLowerCase().contains(key.toLowerCase())
					|| farmer.get(i).getPersonal_info().getName().toLowerCase().contains(key.toLowerCase())
					|| farmer.get(i).getPersonal_info().getPhone().toLowerCase().contains(key.toLowerCase())) {
				System.out.println("In....if......" + key);
				Farmer farm = farmer.get(i);
				farmerList.add(farm);
			}
		}
		return farmerList;
	}

	public List<Consumer> searchConsumer(String key) {
		List<Consumer> consumerList = new ArrayList<>();
		System.out.println("In....searchConsume " + key);
		if(key == null){
			for (int i = 0; i < consumer.size(); i++) {
                                Consumer con = consumer.get(i);
                                consumerList.add(con);
                        }
                }
		else{
			for (int i = 0; i < consumer.size(); i++) {
				if (consumer.get(i).getEmail().toLowerCase().contains(key.toLowerCase())
					|| consumer.get(i).getName().toLowerCase().contains(key.toLowerCase())
					|| consumer.get(i).getPhone().toLowerCase().contains(key.toLowerCase())
					|| consumer.get(i).getStreet().toLowerCase().contains(key.toLowerCase())
					|| consumer.get(i).getZip().toLowerCase().contains(key.toLowerCase())) {
				System.out.println("In....if......" + key);
				Consumer con = consumer.get(i);
				consumerList.add(con);
				}
			}
		}
		return consumerList;
	}

	public void updateDelivery(int oid) {
		Iterator<Order> oo = order.listIterator();
		oid = oid - 1;
		System.out.println("oid for list is--- " + oid);
		while (oo.hasNext()) {
			Order o = oo.next();
			System.out.println("--- OOOOOOOOOOO------" + o.getOid());
			if (o.matchesOid(String.valueOf(oid + 1))) {
				o.setActual_delivery_date(o.getActual_delivery_date());
				o.setCid(o.getCid());
				o.setDelivery_note(o.getDelivery_note());
				o.setFid(o.getFid());
				o.setOid(String.valueOf(oid + 1));
				o.setOrder_date(o.getOrder_date());
				o.setOrder_detail(o.getOrder_detail());
				o.setPlanned_delivery_date(o.getPlanned_delivery_date());
				o.setStatus("delivered");
				order.set(oid, o);
			}
		}
		oid = oid + 1;
	}

	public GCPIDPojo addProductToCatalog(Catalog cr) {
		int gcpid = UniqueIdGenerator.getCatalogID();
		System.out.println("Unique id for Catalog is ---:: " + gcpid);
		cr.setGcpid(String.valueOf(gcpid));
		catalog.add(cr);
		GCPIDPojo id = new GCPIDPojo();
		id.setGcpid(String.valueOf(gcpid));
		return id;
	}

	public List<Catalog> viewProductFromCatalog() {
		List<Catalog> list = new ArrayList<>();
		Iterator<Catalog> cr = catalog.listIterator();
		while (cr.hasNext()) {
			Catalog o = cr.next();
			list.add(o);
		}
		return list;
	}

	public void updateProductFromCatalog(Catalog ct, int id) {
		id = id - 1;
		ct.setGcpid((String.valueOf(id + 1)));
		catalog.set(id, ct);
		id = id + 1;
	}

	public List<ManagerAccount> viewManagerAccount() {
		ManagerAccount ma = new ManagerAccount();
		ma.setCreate_date("20161001");
		ma.setCreated_by("System");
		ma.setEmail("superuser@example.com");
		ma.setMid("0");
		ma.setName("Super User");
		ma.setPhone("123-0987-654");
		manager.add(ma);
		return manager;
	}

	public ManagerAccount viewManagerAccountById(int mid) {
		Iterator<ManagerAccount> ma = manager.listIterator();
		while (ma.hasNext()) {
			ManagerAccount o = ma.next();
			if (o.matchesMid(String.valueOf(mid))) {
				return o;
			}
		}
		return (new NullManagerAccount());
	}

	public List<ManagerReport> viewManagerReport() {
		if (manager_report.size() == 0) {
			ManagerReport mr = new ManagerReport();
			mr.setMrid("1");
			mr.setName("Orders placed yesterday");
			manager_report.add(mr);
			mr = new ManagerReport();
			mr.setMrid("2");
			mr.setName("Revenue for previous month");
			manager_report.add(mr);
			mr = new ManagerReport();
			mr.setMrid("3");
			mr.setName("Revenue yesterday");
			manager_report.add(mr);
			mr = new ManagerReport();
			mr.setMrid("4");
			mr.setName("Revenue yesterday by zip code");
			manager_report.add(mr);
		}
		return manager_report;
	}

	public Report_Yesterday viewYesterDayReport(int mrid, String st_date, String end_date) {
		Report_Yesterday ry = new Report_Yesterday();
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = new GregorianCalendar();
		calendar.add(Calendar.DATE, -1);
		String dateStr = df.format(calendar.getTime());
		int orderplaced = 0;
		int orderdelivered = 0;
		int ordercancelled = 0;
		int orderopem = 0;
		for (int i = 0; i < order.size(); i++) {
			if (st_date == null && end_date == null) {
				if (dateStr.equals(order.get(i).getOrder_date())) {
					orderplaced++;
					if (order.get(i).getStatus().equalsIgnoreCase("open")) {
						orderopem++;
					}
					if (order.get(i).getStatus().equalsIgnoreCase("delivered")) {
						orderdelivered++;
					}
					if (order.get(i).getStatus().equalsIgnoreCase("cancelled")) {
						ordercancelled++;
					}
				}
			} else {
				try {
					Date orderdate = df.parse(order.get(i).getOrder_date());
					Date sdate = df.parse(st_date);
					Date edate = df.parse(end_date);
					if (orderdate.after(sdate) || orderdate.before(edate)) {
						orderplaced++;
						if (order.get(i).getStatus().equalsIgnoreCase("open")) {
							orderopem++;
						}
						if (order.get(i).getStatus().equalsIgnoreCase("delivered")) {
							orderdelivered++;
						}
						if (order.get(i).getStatus().equalsIgnoreCase("cancelled")) {
							ordercancelled++;
						}
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		ry.setMrid(String.valueOf(mrid));
		ry.setName("Orders placed yesterday");
		ry.setOrders_cancelled(ordercancelled);
		ry.setOrders_delivered(orderdelivered);
		ry.setOrders_open(orderopem);
		ry.setOrders_placed(orderplaced);
		return ry;
	}
}
