package edu.iit.cs445.f2016.lf2u;

import java.util.List;

import POJO.CIDPojo;
import POJO.Catalog;
import POJO.Consumer;
import POJO.Delivery_Charge;
import POJO.FSPIDPojo;
import POJO.FamerReportID;
import POJO.Farmer;
import POJO.FarmerReport;
import POJO.GCPIDPojo;
import POJO.IDPojo;
import POJO.ManagerAccount;
import POJO.ManagerReport;
import POJO.OIDPojo;
import POJO.Order;
import POJO.OrderByID;
import POJO.OrderDetail;
import POJO.Product;
import POJO.ProductWithFSP;
import POJO.Report_Yesterday;
import POJO.RevenueReport;

public interface BoundaryInterface {
	public IDPojo createAccount(Farmer far,int pinsize);
	public IDPojo updateAccount(Farmer far,int id,int pinsize);
	public List<Farmer> viewAllFarmers();
	public Farmer viewFarmer(int id);
	public List<Farmer> viewFarmerByZipcode(String zipcode);
	public FSPIDPojo addProductFromCatalog(Product pr, int fid);
	public FSPIDPojo updateProductFromCatalog(Product pr, int fid, int fspid);
	public List<ProductWithFSP> viewAllProductInStore(int fid);
	public List<FarmerReport> viewFarmerReport(int fid);
	public CIDPojo createCustomerAccount(Consumer cr);
	public CIDPojo updateCustomerAccount(Consumer cr, int cid);
	public Consumer viewCustomer(int cid);
	public OIDPojo createOrder(Order or,int id);
	public List<OrderDetail> viewOrder(int cid);
	public void cancelOrder(int oid);
	public List<Farmer> searchFarmer(String key);
	public List<Consumer> searchConsumer(String key);
	public void updateDelivery(int oid);
	public GCPIDPojo addProductToCatalog(Catalog cr);
	public List<Catalog> viewProductFromCatalog();
	public void updateProductFromCatalog(Catalog ct,int id);
	public List<ManagerAccount> viewManagerAccount();
	public ManagerAccount viewManagerAccountById(int mid);
	public FamerReportID viewFarmerReportById(int fid, int frid);
	public RevenueReport viewRevenueReport(int fid, int frid,String startdate,String enddate);
	public void createDeliveryCharge(Delivery_Charge dc,int fid);
	public Delivery_Charge viewCharge(int fid);
	public List<ManagerReport> viewManagerReport();
	public OrderByID viewOrderById(int oid);
	public Report_Yesterday viewYesterDayReport(int mrid,String st_date,String end_date);
}
