package edu.iit.cs445.f2016.lf2u;

import java.util.concurrent.atomic.AtomicInteger;

public class UniqueIdGenerator {
    private static AtomicInteger atomicInteger = new AtomicInteger();
    private static AtomicInteger atomicIntegerConsumer = new AtomicInteger();
    private static AtomicInteger atomicIntegerOrder = new AtomicInteger();
    private static AtomicInteger atomicIntegerProduct = new AtomicInteger();
    private static AtomicInteger atomicIntegerCatalog = new AtomicInteger();
    
    public static int getUniqueID() {
        return atomicInteger.addAndGet(1);
    }
    public static int getConsumerID(){
    	return atomicIntegerConsumer.addAndGet(1);
    }
    public static int getOrderID(){
    	return atomicIntegerOrder.addAndGet(1);
    }
    public static int getCatalogID(){
    	return atomicIntegerCatalog.addAndGet(1);
    }
    public static int getProductID(){
    	return atomicIntegerProduct.addAndGet(1);
    }
}
