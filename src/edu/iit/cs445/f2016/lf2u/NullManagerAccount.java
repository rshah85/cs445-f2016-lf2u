package edu.iit.cs445.f2016.lf2u;

import POJO.ManagerAccount;

public class NullManagerAccount extends ManagerAccount{
    @Override
    public boolean isNil() {
        return true;
    }
}
