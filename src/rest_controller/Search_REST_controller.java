package rest_controller;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import POJO.Consumer;
import POJO.Farmer;
import edu.iit.cs445.f2016.lf2u.BoundaryInterface;
import services.lf2u_services;

@Path("/search")
public class Search_REST_controller {
	private BoundaryInterface bi = new lf2u_services();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response searchData(@QueryParam("topic") String topic, @QueryParam("key") String key) {
		boolean avail_topic = false;		
		System.out.println("In GET....?topic=key");
		if (topic.equals("farm") || topic.equals("customer") || topic.equals("order")) {
			avail_topic = true;
		}		
		if (avail_topic == true) {
			System.out.println("In..if");
			String s = null;
			if (topic.equals("farm")) {
				List<Farmer> far = bi.searchFarmer(key);
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				s = gson.toJson(far);				
			} else if (topic.equals("customer")) {
				List<Consumer> con = bi.searchConsumer(key);
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				s = gson.toJson(con);				
			} else if (topic.equals("order")) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				s = gson.toJson("");				
			}
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			System.out.println("In..else");
			String s = "";
			return Response.status(404).entity(s).build();
		}
	}
}
