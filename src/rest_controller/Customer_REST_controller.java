package rest_controller;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import POJO.CIDOrderPojo;
import POJO.CIDPojo;
import POJO.Consumer;
import POJO.OIDPojo;
import POJO.Order;
import POJO.OrderByID;
import POJO.OrderDetail;
import POJO.OrderStatus;
import POJO.*;
import edu.iit.cs445.f2016.lf2u.BoundaryInterface;
import services.lf2u_services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/customers")
public class Customer_REST_controller {
	private BoundaryInterface bi = new lf2u_services();
	public static List<CIDOrderPojo> cidwithorder = new ArrayList<>();
	public static int cnt = 0;

	// public static List<>
	public StringBuilder ExtractString(InputStream incomingData) {
		StringBuilder jsonInString = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonInString.append(line);
			}
			System.out.println("String-->>" + jsonInString);
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		return jsonInString;
	}

	public JsonNode ExtractJSONNODE(String jsonInString) {
		JsonNode jNode = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			jNode = mapper.readTree(jsonInString.toString());
			System.out.println("--" + jNode);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jNode;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createCustomerAccount(InputStream incomingData, @Context UriInfo uriInfo) throws JsonParseException,
			JsonMappingException, IOException, IllegalAccessException, InvocationTargetException {
		System.out.println("In POST Costumer...");
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder jsonInString = ExtractString(incomingData);
		Consumer far = mapper.readValue(jsonInString.toString(), Consumer.class);
		CIDPojo id = bi.createCustomerAccount(far);
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.path(id.getCid());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String s = gson.toJson(id);
		return Response.created(builder.build()).entity(s).build();
	}

	@Path("{cid}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateCustomerAccount(@PathParam("cid") int id, InputStream incomingData, @Context UriInfo uriInfo)
			throws JsonParseException, JsonMappingException, IOException, IllegalAccessException,
			InvocationTargetException {
		System.out.println("In Put Customer...");
		boolean avail = false;
		System.out.println("In POST Order...");
		Iterator<Consumer> or = lf2u_services.consumer.listIterator();
		while (or.hasNext()) {
			Consumer o = or.next();
			if (o.matchesCid(String.valueOf(id))) {
				avail = true;
			}
		}
		if (avail == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			if (jsonInString.length() == 0)
				return Response.noContent().entity("").build();
			Consumer far = null;
			try {
				far = mapper.readValue(jsonInString.toString(), Consumer.class);
			} catch (Exception e) {
				return Response.status(400).entity("").build();
			}
			CIDPojo idp = null;
			try {
				idp = bi.updateCustomerAccount(far, id);
			} catch (ArrayIndexOutOfBoundsException i) {
				return Response.status(400).entity("").build();
			}
			System.out.println("-->> " + far.getName());
			UriBuilder builder = uriInfo.getAbsolutePathBuilder();
			builder.path(idp.getCid());
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}
	
	@Path("{cid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewCustomer(@PathParam("cid") int cid) {
		System.out.println("In GET....{cid}/orders");
		boolean avail = false;
		Iterator<Consumer> or = lf2u_services.consumer.listIterator();
		while (or.hasNext()) {
			System.out.println("---In ---While"+cid+"  size "+lf2u_services.consumer.size());
			Consumer o = or.next();
			if (o.matchesCid(String.valueOf(cid))) {
				avail = true;
			}			
		}
		System.out.println("In GET....Customer");
		if (avail == true) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			Consumer c = bi.viewCustomer(cid);
			String s = gson.toJson(c);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			System.out.println("In else...");
			// TODO -- Resolve error for not found case....
			return Response.status(404).entity("").build();
		}
	}

	@Path("{cid}/orders")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createOrder(InputStream incomingData, @PathParam("cid") int id, @Context UriInfo uriInfo)
			throws JsonParseException, JsonMappingException, IOException, IllegalAccessException,
			InvocationTargetException {
		boolean avail = false;
		boolean flag = false;
		System.out.println("In POST Order...");
		Iterator<Consumer> or = lf2u_services.consumer.listIterator();
		while (or.hasNext()) {
			Consumer o = or.next();
			if (o.matchesCid(String.valueOf(id))) {
				avail = true;
			}
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String todayDate = sdf.format(new java.util.Date());
		System.out.println("Order Date is ---" + todayDate);
		if (avail == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			Order far = mapper.readValue(jsonInString.toString(), Order.class);
			String farmerid = far.getFid();
			Iterator<FarmWithID> orrr = lf2u_services.farmerwithId.listIterator();
                	ArrayList<String> list = new ArrayList();
			while (orrr.hasNext()) {
                        	FarmWithID o = orrr.next();
                        	if (o.matchesFid(farmerid)) {
                                	list = o.getFarmer().getDelivers_to();
                        	}
                	}
			String zip = null;
			Iterator<Consumer> orr= lf2u_services.consumer.listIterator();
                	while (orr.hasNext()) {
                        	Consumer o = orr.next();
                        	if (o.matchesCid(String.valueOf(id))) {
                                	zip = o.getZip();
                        	}
                	}	
			UriBuilder builder = uriInfo.getAbsolutePathBuilder();
			String s = null;
			for(int i=0;i<list.size();i++){				
				if(zip.equals(list.get(i))){
					flag = true;
				}
			}
			if(cnt == 1){		
				OIDPojo oid = bi.createOrder(far,id);
                        	CIDOrderPojo cid = new CIDOrderPojo();
                       		cid.setCid(String.valueOf(id));
                      	 	cid.setOrder(far);
                        	cid.setOrder_date(todayDate);
                        	cidwithorder.add(cid);                        		
                        	builder.path(oid.getOid());
                	        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        	                s = gson.toJson(oid);	                        	
			}else{
				 cnt++;
				return Response.status(422).entity("").build();	
			}
			return Response.created(builder.build()).entity(s).build();	
		} else {
			return Response.status(422).entity("").build();
		}
	}

	@Path("{cid}/orders")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewOrder(@PathParam("cid") int cid) {
		System.out.println("In GET....{cid}/orders");
		boolean avail = false;
		Iterator<Consumer> or = lf2u_services.consumer.listIterator();
		while (or.hasNext()) {
			Consumer o = or.next();
			if (o.matchesCid(String.valueOf(cid))) {
				avail = true;
			}
		}
		System.out.println("In GET....");
		if (avail == true) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			List<OrderDetail> list = bi.viewOrder(cid);
			String s = gson.toJson(list);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			System.out.println("In else...");
			// TODO -- Resolve error for not found case....
			return Response.status(404).entity("").build();
		}
	}

	@Path("{cid}/orders/{oid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewCustomerOrder(@PathParam("cid") int cid, @PathParam("oid") int oid) {
		boolean avail = false;
		boolean avail_order = false;
		Iterator<Consumer> or = lf2u_services.consumer.listIterator();
		while (or.hasNext()) {
			Consumer o = or.next();
			if (o.matchesCid(String.valueOf(cid))) {
				avail = true;
			}
		}
		Iterator<Order> oo = lf2u_services.order.listIterator();
		while (oo.hasNext()) {
			Order o = oo.next();
			if (o.matchesOid(String.valueOf(oid))) {
				avail_order = true;
			}
		}
		if (avail == true && avail_order == true) {
			System.out.println("Both Avail...");
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			OrderByID ob = bi.viewOrderById(oid);
			String s = gson.toJson(ob);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@Path("{cid}/orders/{oid}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response cancelOrder(InputStream incomingData, @PathParam("cid") int cid, @PathParam("oid") int oid) {
		System.out.println("In POST....{cid}/orders/{oid}");
		boolean avail = false;
		boolean avail_order = false;
		Iterator<Consumer> or = lf2u_services.consumer.listIterator();
		while (or.hasNext()) {
			Consumer o = or.next();
			if (o.matchesCid(String.valueOf(cid))) {
				System.out.println("Consumer Availll....");
				avail = true;
			}
		}

		Iterator<Order> order = lf2u_services.order.listIterator();
		while (order.hasNext()) {
			Order o = order.next();
			if (o.matchesOid(String.valueOf(oid))) {
				System.out.println("Order Availll....");
				avail_order = true;
			}
		}
		if (avail == true && avail_order == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			OrderStatus far = null;
			try {
				far = mapper.readValue(jsonInString.toString(), OrderStatus.class);
				bi.cancelOrder(oid);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Response.status(Response.Status.NO_CONTENT).entity("").build();
		} else {
			return Response.status(400).entity("").build();
		}
	}

	@PostConstruct
	public void postConstruct() {
		// This method gets executed exactly once, after the servlet container
		// has been created
		// A good place to place code that needs to be executed once, at startup
	}
}
