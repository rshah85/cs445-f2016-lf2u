package rest_controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import POJO.Catalog;
import POJO.GCPIDPojo;
import POJO.ManagerAccount;
import POJO.ManagerReport;
import POJO.Report_Yesterday;
import edu.iit.cs445.f2016.lf2u.BoundaryInterface;
import services.lf2u_services;

@Path("/managers")
public class Manager_REST_controller {
	private BoundaryInterface bi = new lf2u_services();

	public StringBuilder ExtractString(InputStream incomingData) {
		StringBuilder jsonInString = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonInString.append(line);
			}
			System.out.println("String-->>" + jsonInString);
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		return jsonInString;
	}

	
	@POST
	@Path("/catalog")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addProductToCatalog(InputStream incomingData, @Context UriInfo uriInfo) throws JsonParseException,
			JsonMappingException, IOException, IllegalAccessException, InvocationTargetException {
		System.out.println("In POST...Add Product to Catalog");
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder jsonInString = ExtractString(incomingData);
		Catalog far = mapper.readValue(jsonInString.toString(), Catalog.class);
		GCPIDPojo id = bi.addProductToCatalog(far);
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
                builder.path(id.getGcpid());
		String jsonOutIdString = new String();
	  	jsonOutIdString = mapper.writeValueAsString(id);
		//Gson gson = new GsonBuilder().setPrettyPrinting().create();
		//String s = gson.toJson(id);
		//return Response.created(builder.build()).entity(s).build();
		//return Response.created(builder.build()).entity(jsonOutIdString).build();
		return Response.status(201).entity(jsonOutIdString).build();
	}

	@Path("/catalog")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewProductFromCatalog() {
		System.out.println("In GET....catalog");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		List<Catalog> list = bi.viewProductFromCatalog();
		String s = gson.toJson(list);
		return Response.status(Response.Status.OK).entity(s).build();
	}

	@Path("/catalog/{gcpid}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updteProductToCatalog(@PathParam("gcpid") int id, InputStream incomingData,
			@Context UriInfo uriInfo) throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, InvocationTargetException {
		System.out.println("In PUT...Update Product to Catalog");
		boolean avail = false;
		Iterator<Catalog> ct = lf2u_services.catalog.listIterator();
		while (ct.hasNext()) {
			Catalog o = ct.next();
			if (o.matchesGcpid(String.valueOf(id))) {
				avail = true;
			}
		}
		if (avail == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			Catalog far = mapper.readValue(jsonInString.toString(), Catalog.class);
			bi.updateProductFromCatalog(far, id);
			return Response.status(Response.Status.OK).entity("").build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@Path("/accounts")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewManagers(@Context UriInfo uriInfo) throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, InvocationTargetException {
		System.out.println("In GET....Managers...");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		List<ManagerAccount> list = bi.viewManagerAccount();
		String s = gson.toJson(list);
		return Response.status(Response.Status.OK).entity(s).build();
	}

	@Path("/accounts/{mid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewManagersById(@PathParam("mid") int mid, @Context UriInfo uriInfo) throws JsonParseException,
			JsonMappingException, IOException, IllegalAccessException, InvocationTargetException {
		boolean avail = false;
		Iterator<ManagerAccount> ma = lf2u_services.manager.listIterator();
		while (ma.hasNext()) {
			ManagerAccount o = ma.next();
			if (o.matchesMid(String.valueOf(mid))) {
				avail = true;
			}
		}
		System.out.println("In GET....view Managers By Id");
		if (avail == true) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			ManagerAccount macct = bi.viewManagerAccountById(mid);
			String s = gson.toJson(macct);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}
	
	@Path("/reports")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewReportType(@Context UriInfo uriInfo) throws JsonParseException,
	JsonMappingException, IOException, IllegalAccessException, InvocationTargetException{
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		List<ManagerReport> macct = bi.viewManagerReport();
		String s = gson.toJson(macct);
		return Response.status(Response.Status.OK).entity(s).build();
	}
	
	@Path("/reports/{mrid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewReportYesterday(@PathParam("mrid") int mrid,@Context UriInfo uriInfo,@QueryParam("start_date") String st_date,@QueryParam("end_date") String end_date) throws JsonParseException,
	JsonMappingException, IOException, IllegalAccessException, InvocationTargetException{
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		Report_Yesterday macct = null;
		String s = null;
		if(mrid == 1){
			macct = bi.viewYesterDayReport(mrid, st_date, end_date);
			s = gson.toJson(macct);
		}else if(mrid == 2){
			
		}
		return Response.status(Response.Status.OK).entity(s).build();
	}
}
