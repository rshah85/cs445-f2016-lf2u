package rest_controller;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;

import POJO.Catalog;
import POJO.Delivery_Charge;
import POJO.FSPIDPojo;
import POJO.FamerReportID;
import POJO.FarmNameId;
import POJO.FarmWithID;
import POJO.Farmer;
import POJO.FarmerReport;
import POJO.IDPojo;
import POJO.Product;
import POJO.ProductPrice;
import POJO.ProductWithFSP;
import POJO.RevenueReport;
import POJO.*;
import edu.iit.cs445.f2016.lf2u.BoundaryInterface;
import services.lf2u_services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/farmers")
public class Farmer_REST_controller {
	private BoundaryInterface bi = new lf2u_services();
	public static List<FarmNameId> farmnameidList = new ArrayList<>();

	// public static List<>
	public StringBuilder ExtractString(InputStream incomingData) {
		StringBuilder jsonInString = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonInString.append(line);
			}
			System.out.println("String-->>" + jsonInString);
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		return jsonInString;
	}

	public JsonNode ExtractJSONNODE(String jsonInString) {
		JsonNode jNode = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			jNode = mapper.readTree(jsonInString.toString());
			System.out.println("--" + jNode);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jNode;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createAccount(InputStream incomingData, @Context UriInfo uriInfo) throws JsonParseException,
			JsonMappingException, IOException, IllegalAccessException, InvocationTargetException {
		System.out.println("In POST...");
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder jsonInString = ExtractString(incomingData);
		Farmer far = mapper.readValue(jsonInString.toString(), Farmer.class);
		int pin_size = far.getDelivers_to().size();
		IDPojo id = bi.createAccount(far, pin_size);
		FarmNameId fni = new FarmNameId();
		fni.setFid(String.valueOf(id.getId()));
		fni.setName(far.getPersonal_info().getName());
		farmnameidList.add(fni);
		UriBuilder builder = uriInfo.getAbsolutePathBuilder();
		builder.path(id.getId());
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String s = gson.toJson(id);
		return Response.created(builder.build()).entity(s).build();
	}

	@Path("{fid}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateAccount(@PathParam("fid") int id, InputStream incomingData, @Context UriInfo uriInfo)
			throws JsonParseException, JsonMappingException, IOException, IllegalAccessException,
			InvocationTargetException {
		System.out.println("In Put...");
		boolean avail = false;
		Iterator<FarmWithID> or = lf2u_services.farmerwithId.listIterator();
		while (or.hasNext()) {
			FarmWithID o = or.next();
			if (o.matchesFid(String.valueOf(id))) {
				avail = true;
			}
		}
		if (avail == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			if (jsonInString.length() == 0)
				return Response.noContent().entity("").build();
			Farmer far = null;
			try {
				far = mapper.readValue(jsonInString.toString(), Farmer.class);
			} catch (Exception e) {
				return Response.status(400).entity("").build();
			}
			// JsonNode node = ExtractJSONNODE(jsonInString.toString());
			// if (node.get("id") == null || node.size() > 2) {
			// System.out.println("--In first if--");
			// return Response.status(400).entity("").build();
			// }
			int pin_size = far.getDelivers_to().size();
			IDPojo idp = null;
			try {
				idp = bi.updateAccount(far, id, pin_size);
			} catch (ArrayIndexOutOfBoundsException i) {
				return Response.status(400).entity("").build();
			}
			FarmNameId fni = new FarmNameId();
			fni.setFid(String.valueOf(idp.getId()));
			fni.setName(far.getPersonal_info().getName());
			id = id - 1;
			farmnameidList.set(id, fni);
			id = id + 1;
			System.out.println("-->> " + far.getFarm_info().getName());
			UriBuilder builder = uriInfo.getAbsolutePathBuilder();
			builder.path(idp.getId());
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@Path("{fid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewFarmers(@PathParam("fid") int id) {
		boolean avail = false;
		Iterator<FarmWithID> or = lf2u_services.farmerwithId.listIterator();
		while (or.hasNext()) {
			FarmWithID o = or.next();
			if (o.matchesFid(String.valueOf(id))) {
				avail = true;
			}
		}
		if (avail == true) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			Farmer far = bi.viewFarmer(id);
			FarmerID fwid = new FarmerID();
			fwid.setFid(String.valueOf(id));
			farm_info fi = new farm_info();
                	ArrayList<String> list = new ArrayList<String>();
                	fi.setName(far.getFarm_info().getName());
                	fi.setAddress(far.getFarm_info().getAddress());
                	fi.setPhone(far.getFarm_info().getPhone());
                	fi.setWeb(far.getFarm_info().getWeb());
                	personal_info pi = new personal_info();
                	pi.setName(far.getPersonal_info().getName());
                	pi.setEmail(far.getPersonal_info().getEmail());
                	pi.setPhone(far.getPersonal_info().getPhone());
                	for (int i = 0; i < far.getDelivers_to().size(); i++) {
                        	list.add(far.getDelivers_to().get(i));
                	}
                	fwid.setFarm_info(fi);
                	fwid.setPersonal_info(pi);
                	fwid.setDelivers_to(list);
			String s = gson.toJson(fwid);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewFarmerByZip(@QueryParam("zip") String zip) {
		boolean noparam = false;
		if (zip == null) {
			noparam = true;
		}
		if (noparam) {
			System.out.println("In get All farmers");
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			List<Farmer> farmerByZip = new ArrayList<>();
			List<FarmNameId> fniList = new ArrayList<>();
			farmerByZip = bi.viewAllFarmers();
			for (int i = 0; i < farmerByZip.size(); i++) {
				FarmNameId fi = new FarmNameId();
				String farmname = farmerByZip.get(i).getPersonal_info().getName();

				String fid = null;
				for (int j = 0; j < farmnameidList.size(); j++) {
					if (farmname.equals(farmnameidList.get(j).getName())) {
						fid = farmnameidList.get(j).getFid();
					}
				}
				fi.setFid(fid);
				fi.setName(farmerByZip.get(i).getPersonal_info().getName());
				fniList.add(fi);
			}
			String s = gson.toJson(fniList);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			System.out.println("In get All farmers by zip");
			List<Farmer> farmerByZip = new ArrayList<>();
			List<FarmNameId> fniList = new ArrayList<>();
			farmerByZip = bi.viewFarmerByZipcode(zip);
			for (int i = 0; i < farmerByZip.size(); i++) {
				FarmNameId fi = new FarmNameId();
				String farmname = farmerByZip.get(i).getPersonal_info().getName();
				String fid = null;
				for (int j = 0; j < farmnameidList.size(); j++) {
					if (farmname.equals(farmnameidList.get(j).getName())) {
						fid = farmnameidList.get(j).getFid();
					}
				}
				fi.setFid(fid);
				fi.setName(farmerByZip.get(i).getPersonal_info().getName());
				fniList.add(fi);
			}
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String s = gson.toJson(fniList);
			return Response.status(Response.Status.OK).entity(s).build();
		}
	}

	@Path("{fid}/products")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addProduct(InputStream incomingData, @PathParam("fid") int fid, @Context UriInfo uriInfo)
			throws JsonParseException, JsonMappingException, IOException, IllegalAccessException,
			InvocationTargetException {
		System.out.println("In POST...fid/products");
		boolean avail = false;
		Iterator<FarmWithID> fr = lf2u_services.farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(String.valueOf(fid))) {
				avail = true;
			}
		}		
		if (avail == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			Product pr = mapper.readValue(jsonInString.toString(), Product.class);
			System.out.println("GCP id to check is ::  --- "+pr.getGcpid());
			boolean prod_avail = false;
			Iterator<Catalog> ct = lf2u_services.catalog.listIterator();
			while(ct.hasNext()){
				Catalog c = ct.next();
				if(c.matchesGcpid(pr.getGcpid())){
					prod_avail = true;
				}
			}
			if(prod_avail == true){
			FSPIDPojo id = bi.addProductFromCatalog(pr, fid);
			UriBuilder builder = uriInfo.getAbsolutePathBuilder();
			builder.path(id.getFspid());
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String s = gson.toJson(id);
			return Response.created(builder.build()).entity(s).build();
			}else{
				return Response.status(404).entity("").build();
			}
		} else {
			return Response.status(404).entity("").build();
		}
	}
	

	@Path("{fid}/products/{fspid}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProduct(InputStream incomingData, @PathParam("fid") int fid, @PathParam("fspid") int fspid,
			@Context UriInfo uriInfo) throws JsonParseException, JsonMappingException, IOException,
			IllegalAccessException, InvocationTargetException {
		System.out.println("In PUT...fid/products/fspid");
		boolean avail = false;
		Iterator<FarmWithID> fr = lf2u_services.farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(String.valueOf(fid))) {
				avail = true;
			}
		}
		boolean avail_product = false;
		Iterator<ProductWithFSP> pr = lf2u_services.productFSPList.listIterator();
		while (pr.hasNext()) {
			ProductWithFSP f = pr.next();
			if (f.matchesFspid(String.valueOf(fspid))) {
				avail_product = true;
			}
		}
		if (avail == true && avail_product == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			Product pr1 = mapper.readValue(jsonInString.toString(), Product.class);
			FSPIDPojo id = bi.updateProductFromCatalog(pr1, fid, fspid);
			UriBuilder builder = uriInfo.getAbsolutePathBuilder();
			builder.path(id.getFspid());
			return Response.status(Response.Status.OK).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@Path("{fid}/products")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewAllProductInStore(@PathParam("fid") int fid) {
		System.out.println("In GET....fid/products");
		boolean avail = false;
		Iterator<FarmWithID> fr = lf2u_services.farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(String.valueOf(fid))) {
				avail = true;
			}
		}
		if (avail == true) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			List<ProductWithFSP> list = bi.viewAllProductInStore(fid);
			String s = gson.toJson(list);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@Path("{fid}/reports")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewFarmerReport(@PathParam("fid") int fid) {
		System.out.println("In GET....fid/reports");
		boolean avail = false;
		Iterator<FarmWithID> fr = lf2u_services.farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(String.valueOf(fid))) {
				avail = true;
			}
		}
		if (avail == true) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			List<FarmerReport> list = bi.viewFarmerReport(fid);
			String s = gson.toJson(list);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@Path("{fid}/reports/{frid}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewFarmerReportWithDate(@PathParam("fid") int fid, @PathParam("frid") int frid,
			@QueryParam("start_date") String startDate, @QueryParam("end_date") String endDate)
			throws JsonProcessingException, IllegalAccessException, InvocationTargetException {

		System.out.println("In GET....fid/reports/frid");
		boolean avail = false;
		Iterator<FarmWithID> fr = lf2u_services.farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(String.valueOf(fid))) {
				avail = true;
			}
		}
		boolean avail_frid = false;
		Iterator<FarmerReport> ff = lf2u_services.report.listIterator();
		while (ff.hasNext()) {
			FarmerReport ft = ff.next();
			if (ft.matchesFrid(String.valueOf(frid))) {
				avail_frid = true;
			}
		}
		if (String.valueOf(frid).equals("1")) {
			if (avail == true && avail_frid == true) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				FamerReportID fo = bi.viewFarmerReportById(fid, frid);
				String s = gson.toJson(fo);
				return Response.status(Response.Status.OK).entity(s).build();
			} else {
				return Response.status(404).entity("").build();
			}
		}else if(String.valueOf(frid).equals("2")){
			if (avail == true && avail_frid == true) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				FamerReportID fo = bi.viewFarmerReportById(fid, frid);
				String s = gson.toJson(fo);
				return Response.status(Response.Status.OK).entity(s).build();
			} else {
				return Response.status(404).entity("").build();
			}
		}
		else if (String.valueOf(frid).equals("3")) {
			if (avail == true && avail_frid == true) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				RevenueReport fo = bi.viewRevenueReport(fid, frid, startDate, endDate);
				String s = gson.toJson(fo);
				return Response.status(Response.Status.OK).entity(s).build();
			} else {
				return Response.status(404).entity("").build();
			}
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@Path("{fid}/delivery_charge")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postDelivery(InputStream incomingData, @PathParam("fid") int fid, @Context UriInfo uriInfo) {
		boolean avail = false;
		Iterator<FarmWithID> fr = lf2u_services.farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(String.valueOf(fid))) {
				avail = true;
			}
		}
		if (avail == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			Delivery_Charge pr1 = null;
			try {
				pr1 = mapper.readValue(jsonInString.toString(), Delivery_Charge.class);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			bi.createDeliveryCharge(pr1, fid);

			return Response.status(Response.Status.NO_CONTENT).build();
		} else {
			return Response.status(400).entity("").build();
		}
	}

	@Path("/{fid}/delivery_charge")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response viewDeliveryCharge(@PathParam("fid") int fid) {
		boolean avail = false;
		Iterator<FarmWithID> fr = lf2u_services.farmerwithId.listIterator();
		while (fr.hasNext()) {
			FarmWithID f = fr.next();
			if (f.matchesFid(String.valueOf(fid))) {
				avail = true;
			}
		}
		if (avail == true) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			Delivery_Charge fo = bi.viewCharge(fid);
			String s = gson.toJson(fo);
			return Response.status(Response.Status.OK).entity(s).build();
		} else {
			return Response.status(404).entity("").build();
		}
	}

	@PostConstruct
	public void postConstruct() {
		// This method gets executed exactly once, after the servlet container
		// has been created
		// A good place to place code that needs to be executed once, at startup
	}
}
