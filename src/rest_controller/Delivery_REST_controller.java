package rest_controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonParseException;
import POJO.Order;
import POJO.OrderStatus;
import edu.iit.cs445.f2016.lf2u.BoundaryInterface;
import services.lf2u_services;

@Path("/delivery")
public class Delivery_REST_controller {
	private BoundaryInterface bi = new lf2u_services();

	public StringBuilder ExtractString(InputStream incomingData) {
		StringBuilder jsonInString = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				jsonInString.append(line);
			}
			System.out.println("String-->>" + jsonInString);
		} catch (Exception e) {
			System.out.println("Error Parsing: - ");
		}
		return jsonInString;
	}

	@Path("{oid}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateDelivery(InputStream incomingData, @PathParam("oid") int oid, @Context UriInfo uriInfo)
			throws JsonParseException, JsonMappingException, IOException, IllegalAccessException,
			InvocationTargetException {
		System.out.println("In POST Delivery...");
		boolean avail_order = false;
		
		Iterator<Order> order = lf2u_services.order.listIterator();
		while (order.hasNext()) {
			Order o = order.next();
			if (o.matchesOid(String.valueOf(oid))) {
				System.out.println("Order Availll....");
				avail_order = true;
			}
		}
		if (avail_order == true) {
			ObjectMapper mapper = new ObjectMapper();
			StringBuilder jsonInString = ExtractString(incomingData);
			OrderStatus far = null;
			try {
				far = mapper.readValue(jsonInString.toString(), OrderStatus.class);
				bi.updateDelivery(oid);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Response.status(Response.Status.NO_CONTENT).entity("").build();
		} else {
			return Response.status(404).entity("").build();
		}
	}
}
