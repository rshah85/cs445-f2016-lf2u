package unit_test;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import POJO.Catalog;
import POJO.GCPIDPojo;
import POJO.ManagerAccount;
import POJO.ManagerReport;
import edu.iit.cs445.f2016.lf2u.NullManagerAccount;
import services.lf2u_services;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ManagersUnitTest {
	
	public Catalog catalog = null;
	public ManagerAccount manager = null;
	
	@Before
	public void createCatalog(){
		catalog = new Catalog();
		catalog.setGcpid("1");
		catalog.setName("Honey");
	}
	
	@Test
	public void stage1_testAddProductToCatalog(){
		System.out.println("Manager...1");
		int size = lf2u_services.catalog.size();
		assertEquals(size, 0);
		lf2u_services service = new lf2u_services();
		GCPIDPojo id = service.addProductToCatalog(catalog);
		size = lf2u_services.catalog.size();
		assertEquals(size, 1);		
		String gcpid = id.getGcpid();
		assertEquals(gcpid, "1");
		boolean check = false;
		if(lf2u_services.catalog.get(0).getName().equals("Honey")){
			check = true;
		}
		assertEquals(check, true);
	}
	
	@Test
	public void stage2_testViewCatalog(){
		System.out.println("Manager...2");
		List<Catalog> ctList = new ArrayList<>();
		int size = ctList.size();
		assertEquals(size, 0);
		lf2u_services service = new lf2u_services();
		ctList = service.viewProductFromCatalog();
		size = ctList.size();
		assertEquals(size, 1);
	}
	
	@Test
	public void stage3_testUpdateProductFromCatalog(){
		System.out.println("Manager...3");
		int size = lf2u_services.catalog.size();
		assertEquals(size, 1);
		catalog = new Catalog();
		catalog.setGcpid("1");
		catalog.setName("Beans");
		lf2u_services service = new lf2u_services();
		service.updateProductFromCatalog(catalog, 1);
		boolean check = false;
		if(lf2u_services.catalog.get(0).getName().equals("Beans")){
			check = true;
		}
		assertEquals(check, true);
	}
	
	@Test
	public void stage4_testviewManagerAccount(){
		System.out.println("Manager...4");
		int size = lf2u_services.manager.size();
		assertEquals(size, 0);		
		lf2u_services service = new lf2u_services();
		List<ManagerAccount> list = new ArrayList<>();
		size = list.size();
		assertEquals(size, 0);
		list = service.viewManagerAccount();
		size = list.size();
		assertEquals(size, 1);
		boolean check = false;
		if(list.get(0).getName().equals("Super User")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(list.get(0).getMid().equals("0")){
			check = true;
		}
		assertEquals(check, true);
	}
	
	@Test(expected = NullPointerException.class)
	public void stage5_testviewManagerAccountById(){
		System.out.println("Manager...5");
		lf2u_services service = new lf2u_services();
		ManagerAccount acct = new ManagerAccount();
		acct = service.viewManagerAccountById(0);
		boolean check = true;
		if(acct.getCreated_by().equals("System")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(acct.getEmail().equals("superuser@example.com")){
			check = true;
		}
		assertEquals(check, true);
		ManagerAccount nullacct = new NullManagerAccount();
		nullacct = service.viewManagerAccountById(1);
		boolean flag = nullacct.isNil();
		assertEquals(flag, true);
		check = false;
		if(nullacct.getEmail().equals("")){
			check = true;
		}
		assertEquals(check, true);		
	}
	
	@Test
	public void stage6_testviewManagerReport(){
		System.out.println("Manager...6");
		lf2u_services service = new lf2u_services();
		List<ManagerReport> list = service.viewManagerReport();
		int size = list.size();
		assertEquals(size, 4);
	}	
}
