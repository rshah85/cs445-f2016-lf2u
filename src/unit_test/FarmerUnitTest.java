package unit_test;

import POJO.Delivery_Charge;
import POJO.FSPIDPojo;
import POJO.FamerReportID;
import POJO.FarmWithID;
import POJO.Farmer;
import POJO.FarmerReport;
import POJO.IDPojo;
import POJO.Product;
import POJO.ProductPrice;
import POJO.ProductWithFSP;
import POJO.farm_info;
import POJO.personal_info;
import POJO.Report_Yesterday;
import edu.iit.cs445.f2016.lf2u.NullFarmer;
import services.lf2u_services;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FarmerUnitTest {
	public Farmer farmer = null;
	public farm_info farm_info = null;
	public personal_info personal_info = null;
	public Product product = null;
	public Product prodprice = null;
	public Delivery_Charge dc = null;
	
	@Before
	public void createFarmerAccount(){
		farmer = new Farmer();
		farm_info = new farm_info();
		personal_info = new personal_info();
		ArrayList<String> delivers = new ArrayList<>();
		delivers.add("60616");
		delivers.add("60100");
		delivers.add("97124");
		farmer.setDelivers_to(delivers);
		farm_info.setAddress("134 Linden Ave ,IL 60616");
		farm_info.setName("my farm");
		farm_info.setPhone("1234567890");
		farm_info.setWeb("");
		personal_info.setEmail("farmer1@gmail.com");
		personal_info.setName("farmer1");
		personal_info.setPhone("0987654321");
		farmer.setFarm_info(farm_info);
		farmer.setPersonal_info(personal_info);		
	}
	
	@Before
	public void createProduct(){
		product = new Product();
		product.setEnd_date("");
		product.setFid("1");
		product.setGcpid("1");
		product.setImage("");
		product.setNote("Fluffy mashed potatoes");
		product.setPrice(0.25);
		product.setProduct_unit("lb");
		product.setStart_date("10-01");
	}
	
	@Before
	public void createCharge(){
		dc = new Delivery_Charge();
		dc.setDelivery_charge(5.5);
	}
	
	@Test
	public void A_testCreateFarmerAccount(){
		System.out.println("farmer 1......");
		FarmWithID fnid = new FarmWithID();
		lf2u_services service = new lf2u_services();
		int size = lf2u_services.farmer.size();
		assertEquals(size, 0);
		int pinsize = farmer.getDelivers_to().size();
		IDPojo id = service.createAccount(farmer, pinsize);
		size = lf2u_services.farmer.size();
		assertEquals(size, 1);
		boolean check = false;
		if(id.getId().equals("1")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.farmer.get(0).getDelivers_to().get(0).equals("60616")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.farmer.get(0).getFarm_info().getName().equals("my farm")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.farmer.get(0).getFarm_info().getAddress().equals("134 Linden Ave ,IL 60616")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.farmer.get(0).getFarm_info().getPhone().equals("1234567890")){
			check = true;
		}
		assertEquals(check, true);
		fnid.setFid(id.getId());
		fnid.setFarmer(farmer);
		String fid = fnid.getFid();
		assertEquals(fid, "1");
	}	
	
	@Test
	public void B_testUpdateFarmerAccount(){
		System.out.println("farmer 2......");
		lf2u_services service = new lf2u_services();
		int size = lf2u_services.farmer.size();
		assertEquals(size, 1);
		boolean check = false;
		if(lf2u_services.farmer.get(0).getFarm_info().getName().equals("my farm")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.farmer.get(0).getFarm_info().getPhone().equals("1234567890")){
			check = true;
		}
		assertEquals(check, true);
		farmer = new Farmer();
		farm_info = new farm_info();
		personal_info = new personal_info();
		ArrayList<String> delivers = new ArrayList<>();
		delivers.add("60616");
		delivers.add("60100");
		delivers.add("97124");
		farmer.setDelivers_to(delivers);
		farm_info.setAddress("134 Linden Ave ,IL 60616");
		farm_info.setName("super farm");
		farm_info.setPhone("1234567890");
		farm_info.setWeb("");
		personal_info.setEmail("farmer1@gmail.com");
		personal_info.setName("farmer1");
		personal_info.setPhone("0987654321");
		farmer.setFarm_info(farm_info);
		farmer.setPersonal_info(personal_info);		
		service.updateAccount(farmer, 1, lf2u_services.farmer.get(0).getDelivers_to().size());
		check = false;
		if(lf2u_services.farmer.get(0).getFarm_info().getName().equals("super farm")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.farmer.get(0).getFarm_info().getPhone().equals("1234567890")){
			check = true;
		}
		assertEquals(check, true);
	}
	
	@Test(expected = NullPointerException.class)
	public void C_testViewFarmerAccount(){
		System.out.println("farmer 3......");
		lf2u_services service = new lf2u_services();
		Farmer far = service.viewFarmer(1);
		boolean check = false;
		if(far.getFarm_info().getName().equals("super farm")){
			check = true;
		}
		assertEquals(check, true);
		int size = lf2u_services.farmer.size();
		assertEquals(size, 1);
		Farmer nullfar = new NullFarmer();
		boolean isNil = false;
		if(nullfar.isNil() == true){
			isNil = true;
		}
		assertEquals(isNil, true);
		check = false;
		if(nullfar.getFarm_info().getName().equals("")){
			check = true;
		}
		assertEquals(check, true);
	}
	
	@Test
	public void D_testViewAllFarmers(){
		System.out.println("farmer 4......");
		lf2u_services service = new lf2u_services();
		List<Farmer> list = new ArrayList<>();
		int size = list.size();
		assertEquals(size, 0);
		list = service.viewAllFarmers();
		size = list.size();
		assertEquals(size, 1);		
		int farmersize = lf2u_services.farmer.size();
		assertEquals(farmersize, 1);
		farmer = new Farmer();
		farm_info = new farm_info();
		personal_info = new personal_info();
		ArrayList<String> delivers = new ArrayList<>();
		delivers.add("60616");
		delivers.add("60100");
		delivers.add("97125");
		farmer.setDelivers_to(delivers);
		farm_info.setAddress("134 Linden Ave ,OR 97125");
		farm_info.setName("super farm 2");
		farm_info.setPhone("1234567890");
		farm_info.setWeb("");
		personal_info.setEmail("farmer2@gmail.com");
		personal_info.setName("farmer2");
		personal_info.setPhone("0987654321");
		farmer.setFarm_info(farm_info);
		farmer.setPersonal_info(personal_info);
		service.createAccount(farmer, delivers.size());
		list = service.viewAllFarmers();
		size = list.size();
		assertEquals(size, 2);	
	}
	
	@Test
	public void E_testViewAllFarmerByZip(){
		System.out.println("farmer 5......");
		lf2u_services service = new lf2u_services();
		List<Farmer> list = new ArrayList<>();
		int size = list.size();
		assertEquals(size, 0);
		list = service.viewFarmerByZipcode("97124");
		size = list.size();
		assertEquals(size, 1);
		boolean check = false;
		if(list.get(0).getFarm_info().getName().equals("super farm")){
			check = true;
		}
		assertEquals(check, true);
		list = service.viewFarmerByZipcode("97125");
		check = false;
		if(list.get(0).getFarm_info().getName().equals("super farm 2")){
			check = true;
		}
		assertEquals(check, true);
		list = service.viewFarmerByZipcode("11111");
		size = list.size();
		assertEquals(size, 0);
	}
	
	@Test
	public void F_testAddProductToStore(){
		System.out.println("farmer 6......");
		lf2u_services service = new lf2u_services();
		int size = lf2u_services.product.size();
		assertEquals(size, 0);
		FSPIDPojo id = service.addProductFromCatalog(product, 1);
		String fspid = id.getFspid();
		assertEquals(fspid, "1");
		boolean check = false;
		if(lf2u_services.product.get(0).getStart_date().equals("10-01")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.product.get(0).getNote().equals("Fluffy mashed potatoes")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.product.get(0).getPrice() == 0.25){
			check = true;
		}
		assertEquals(check, true);
	}	
	
	@Test
	public void G_testUpdateProductFromCatalog(){
		System.out.println("farmer 7......");
		lf2u_services service = new lf2u_services();
		int size = lf2u_services.product.size();
		assertEquals(size, 1);
		boolean check = false;
		if(lf2u_services.product.get(0).getPrice() == 0.25){
			check = true;
		}
		assertEquals(check, true);
		prodprice = new Product();
		prodprice.setPrice(0.35);
		service.updateProductFromCatalog(prodprice, 1, 1);
		check = false;
		if(lf2u_services.product.get(0).getPrice() == 0.35){
			check = true;System.out.println("farmer 7......");
		}
		assertEquals(check, true);
	}
	
	@Test
	public void H_testViewAllProductInStore(){
		System.out.println("farmer 8......");
		lf2u_services service = new lf2u_services();
		List<ProductWithFSP> list = new ArrayList<>();
		int size = list.size();
		assertEquals(size, 0);
		list = service.viewAllProductInStore(1);
		size = list.size();
		assertEquals(size, 1);
		boolean check = false;
		if(list.get(0).getProduct_unit().equals("lb")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(list.get(0).getStart_date().equals("10-01")){
			check = true;
		}
		assertEquals(check, true);		
	}	
	
	@Test
	public void I_testCreateDeliveryCharge(){
		System.out.println("farmer 9......");
		lf2u_services service = new lf2u_services();
		int size = lf2u_services.farmerdelivery.size();
		assertEquals(size, 0);		
		service.createDeliveryCharge(dc, 1);
		size = lf2u_services.farmerdelivery.size();
		assertEquals(size, 1);		
	}		
	
	@Test
	public void J_testViewDeliveryCharge(){
		System.out.println("farmer 10......");
		lf2u_services service = new lf2u_services();
		Delivery_Charge dc = service.viewCharge(1);
		boolean check = false;
		System.out.println("-->> "+dc.getDelivery_charge());
		if(dc.getDelivery_charge() == 5.5){
			check = true;
		}
		assertEquals(check,true);
	}
	
	@Test
	public void K_testViewFarmerReport(){
		System.out.println("farmer 11.....");
		lf2u_services service = new lf2u_services();
		List<FarmerReport> list = new ArrayList<>();
		list = service.viewFarmerReport(1);
		int size = list.size();
		assertEquals(size, 4);
		boolean check = false;
		if(lf2u_services.report.get(0).getName().equals("Orders to deliver today")){
			check = true;
		}
		assertEquals(check,true);
		check = false;
		if(lf2u_services.report.get(0).getFrid().equals("1")){
			check = true;
		}
		assertEquals(check,true);
		check = false;
                if(lf2u_services.report.get(1).getFrid().equals("Orders to deliver tomorrow")){
                        check = true;
                }
                assertEquals(check,false);
		check = false;
                if(lf2u_services.report.get(1).getFrid().equals("2")){
                        check = true;
                }
                assertEquals(check,true);
	}		
	
	@Test
	public void L_testSearchFarmer(){
		System.out.println("farmer 12.....");
		lf2u_services service = new lf2u_services();
		List<Farmer> list = new ArrayList<>();
		list = service.searchFarmer("farm"); // It will return 2 records as we have "super farm" and "super farm 2"
		boolean check = false;	
		System.out.println("--***************------------------"+list.get(0).getFarm_info().getName());
		if(list.get(1).getFarm_info().getName().equals("super farm 2")){
			check = true;
		}
		assertEquals(check, true);
		list = new ArrayList<>();
		list = service.searchFarmer("Linden");
		check = false;
		if(list.get(1).getFarm_info().getAddress().equals("134 Linden Ave ,OR 97125")){
			check = true;
		}
		assertEquals(check, true);
	}
	
	@Test
	public void M_viewReportByID(){
		System.out.println("farmer 13.....");
		lf2u_services service = new lf2u_services();
		FamerReportID id = service.viewFarmerReportById(1, 1);
		boolean check = false;
		if(id.getName().equals("Orders to deliver today")){
			check = true;
		}
		assertEquals(check,true);
		id = service.viewFarmerReportById(1, 2);
                check = false;
                if(id.getName().equals("Orders to deliver tomorrow")){
                        check = true;
                }
                assertEquals(check,true);
	}
	
	@Test
	public void N_viewYesterDayReport(){
		System.out.println("farmer 14....");
		lf2u_services ser = new lf2u_services();
		Report_Yesterday ry = ser.viewYesterDayReport(2, null, null);
		boolean check = false;
		if(ry.getName().equals("Orders placed yesterday")){
			check = true;
		}
		assertEquals(check,true);
	}
}
