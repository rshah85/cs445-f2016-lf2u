package unit_test;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import POJO.CIDPojo;
import POJO.Consumer;
import POJO.FSPAmount;
import POJO.FamerReportID;
import POJO.Order;
import POJO.OrderByID;
import POJO.OrderDetail;
import POJO.RevenueReport;
import services.lf2u_services;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerUnitTest {
	public Consumer consumer = null;
	public Order order = null;
	
	@Before
	public void createNewConsumer(){
		consumer = new Consumer();		
		consumer.setEmail("test@gmail.com");
		consumer.setName("Rahi Shah");
		consumer.setPhone("1234567890");
		consumer.setStreet("1221 NE 51st Ave");
		consumer.setZip("60616");
	}
	
	@Before
	public void createNewOrder(){
		order = new Order();
		order.setActual_delivery_date("");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar c = Calendar.getInstance();
		c.setTime(new java.util.Date());
		c.add(Calendar.DATE, 1);
		order.setOrder_date(sdf.format(new java.util.Date()));
		order.setPlanned_delivery_date(sdf.format(c.getTime()));
		order.setCid("1");
		order.setDelivery_note("Leave at Door...");
		order.setFid("1");
		order.setOid("1");
		ArrayList<FSPAmount> list = new ArrayList<>();
		FSPAmount fspamt = new FSPAmount();
		fspamt.setFspid("1");
		fspamt.setAmount(5);
		list.add(fspamt);
		fspamt = new FSPAmount();
		fspamt.setFspid("2");
		fspamt.setAmount(10);
		list.add(fspamt);
		order.setOrder_detail(list);
		order.setStatus("open");
	}
	
	@Test
	public void A_testCreateCustomerAccount(){
		System.out.println("test1...");
		lf2u_services lf2u_ser = new lf2u_services();
		CIDPojo id = lf2u_ser.createCustomerAccount(consumer);		
		boolean check = false;		
		if(lf2u_services.consumer.get(0).getName().equals("Rahi Shah")){
			check = true;			
		}
		assertEquals(check, true);
		check = false;
		if(lf2u_services.consumer.get(0).getCid().equals(id.getCid())){
			check = true;
		}
		assertEquals(check, true);
		int size = 0;
		size = lf2u_services.consumer.size();
		assertEquals(size, 1);		
	}
	
	@Test
	public void B_testupdateCustomerAccount(){
		lf2u_services lf2u_ser = new lf2u_services();
		System.out.println("test2...");				
		int size = 0;
		size = lf2u_services.consumer.size();
		assertEquals(size, 1);	
		boolean check_before = false;		
		if(lf2u_services.consumer.get(0).getName().equals("Rahi Shah")){
			check_before = true;
		}
		assertEquals(check_before, true);
		consumer = new Consumer();
		consumer.setEmail("test@gmail.com");
		consumer.setName("Yash Shah");
		consumer.setPhone("1234567890");
		consumer.setStreet("1221 NE 51st Ave");
		consumer.setZip("60600");
		CIDPojo id = lf2u_ser.updateCustomerAccount(consumer, 1);
		boolean check = false;
		if(lf2u_services.consumer.get(0).getName().equals("Yash Shah")){
			check = true;
		}
		assertEquals(check, true);
		assertEquals(size, 1);
		check = false;
		if(lf2u_services.consumer.get(0).getCid().equals(id.getCid())){
			check = true;
		}
		assertEquals(check, true);
	}
	
	@Test
	public void C_testViewConsumer(){
		System.out.println("test3...");
		lf2u_services services = new lf2u_services();
		Consumer list = services.viewCustomer(1);
		boolean check = false;
		if(list.getName().equals("Yash Shah"))
			check = true;
		assertEquals(check, true);
	}
	
	@Test
	public void D_testCreateOrder(){
		System.out.println("test4....");
		lf2u_services lf2u_ser = new lf2u_services();
		int size = 0;
		size = lf2u_services.order.size();
		assertEquals(size, 0);
		lf2u_ser.createOrder(order, 1);
		size = lf2u_services.order.size();
		assertEquals(size, 1);
		boolean check = false;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		if(lf2u_services.order.get(0).getOrder_date().equals(sdf.format(new java.util.Date()))){
			check = true;
		}
		assertEquals(check, true);
		String oid = lf2u_services.order.get(0).getOid();
		assertEquals(oid, "1");
	}
	
	@Test
	public void E_testViewOrder(){
		System.out.println("test5.....");
		lf2u_services service = new lf2u_services();
		List<OrderDetail> odList = new ArrayList<>();
		int size = odList.size();
		assertEquals(size, 0);
		odList = service.viewOrder(1);
		size = odList.size();
		assertEquals(size, 1);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar c = Calendar.getInstance();
		c.setTime(new java.util.Date());
		c.add(Calendar.DATE, 1);
		boolean check = false;
		if(odList.get(0).getOrder_date().equals(sdf.format(new java.util.Date()))){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(odList.get(0).getStatus().equals("open")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		if(odList.get(0).getPlanned_delivery_date().equals(sdf.format(c.getTime()))){
			check = true;
		}
		assertEquals(check, true);
		int oid = Integer.parseInt(odList.get(0).getOid());
		assertEquals(oid, 1);
	}
	
	@Test
	public void F_testCancelOrder(){
		System.out.println("test6.....");
		int size = lf2u_services.order.size();
		assertEquals(size, 1);
		boolean check = false;
		if(lf2u_services.order.get(0).getStatus().equals("open")){
			check = true;
		}
		assertEquals(check, true);
		check = false;
		lf2u_services service = new lf2u_services();
		service.cancelOrder(1);
		if(lf2u_services.order.get(0).getStatus().equals("cancelled")){
			check = true;
		}
		assertEquals(check, true);		
	}
	
	@Test
	public void G_testViewFarmerReportByID(){
		System.out.println("test7.....");
		lf2u_services service = new lf2u_services();
		
		//Create order
		service.viewOrder(1);
		service.createOrder(order, 1); // Create order with oid=2
		FamerReportID id = service.viewFarmerReportById(1, 1);
		boolean check = false;		
		if(id.getOrders().get(1).getOid().equals("2")){
			check = true;
		}
		assertEquals(check,true);
		check = false;
		if(id.getFrid().equals("1")){
			check = true;
		}
		assertEquals(check,true);	
		
		//TODO - Check for more than 2 orders
		//service.createOrder(order, 1);
		//service.createOrder(order, 1);// Create order with oid=3		
		//id = service.viewFarmerReportById(1, 701);
		//check = false;		
		//if(id.getOrders().get(2).getOid().equals("3")){
		//	check = true;
		//}
		//assertEquals(check,true);		
	}
	
	@Test
	public void H_testUpdateDelivery(){
		System.out.println("test8.....");
		lf2u_services service = new lf2u_services();
		service.updateDelivery(2);
		boolean check = false;
		if(lf2u_services.order.get(1).getStatus().equals("delivered")){
			check = true;
		}
		assertEquals(check,true);
	}
	
	@Test
	public void I_testViewFarmerRevenueReport(){
		System.out.println("test9.....");
		lf2u_services service = new lf2u_services();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String startdate = sdf.format(new java.util.Date());
		String enddate = sdf.format(new java.util.Date());
		//Total order size
		int totalorder = lf2u_services.order.size();
		RevenueReport rr = service.viewRevenueReport(1, 703, startdate, enddate);
		int orderplaced = rr.getOrders_placed();
		assertEquals(totalorder, orderplaced);
		//Cancel order with oid=1
		int ordercancel = rr.getOrders_cancelled();
		assertEquals(ordercancel, 1);
		String frid = rr.getFrid();
		assertEquals(frid, "703");		
	}
	
	@Test
	public void J_testSearchConsumer(){
		System.out.println("test10.....");
		lf2u_services service = new lf2u_services();
		List<Consumer> list = new ArrayList<>();
		list = service.searchConsumer("Yash");
		boolean check = false;
		if(list.get(0).getName().equals("Yash Shah")){
			check = true;
		}
		assertEquals(check,true);
		check = false;
		if(list.get(0).getStreet().equals("1221 NE 51st Ave")){
			check = true;
		}
		assertEquals(check,true);
		check = false;
		if(list.get(0).getCid().equals("1")){
			check = true;
		}
		assertEquals(check,true);
		check = false;
		if(list.get(0).getPhone().equals("1234567890")){
			check = true;
		}
		assertEquals(check,true);
	}
	
	@Test
	public void K_testviewOrderById(){
		System.out.println("test11.....");
		lf2u_services service = new lf2u_services();
		OrderByID od = service.viewOrderById(1);
		boolean check = false;
		if(od.getDelivery_note().equals("Leave at Door...")){
			check = true;
		}
		assertEquals(check,true);
	}
}
