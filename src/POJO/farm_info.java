package POJO;

public class farm_info {
	public String fid;
	public String name;
	public String address;
	public String phone;
	public String web;

	public String getFid(){
		return fid;
	}	
	
	public void setFid(String fid){
		this.fid = fid;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}		
}
