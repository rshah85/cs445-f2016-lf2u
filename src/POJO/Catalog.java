package POJO;

public class Catalog {
	public String gcpid;
	public String name;
	
	public Catalog(){
		System.out.println("In Catalog....");
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGcpid() {
		return gcpid;
	}
	public void setGcpid(String gcpid) {
		this.gcpid = gcpid;
	}		
	public boolean matchesGcpid(String id){
		return(this.gcpid.equals(id));
	}
}
