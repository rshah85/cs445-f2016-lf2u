package POJO;

import java.util.ArrayList;

public class Order {
	private String fid;
	private ArrayList<FSPAmount> order_detail;
	private String delivery_note;
	private String oid;		
	private String cid;
	private String order_date;
	private String planned_delivery_date;
	private String actual_delivery_date;
	private String status;
			
	public String getOrder_date() {
		return order_date;
	}
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
	public String getPlanned_delivery_date() {
		return planned_delivery_date;
	}
	public void setPlanned_delivery_date(String planned_delivery_date) {
		this.planned_delivery_date = planned_delivery_date;
	}
	public String getActual_delivery_date() {
		return actual_delivery_date;
	}
	public void setActual_delivery_date(String actual_delivery_date) {
		this.actual_delivery_date = actual_delivery_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getFid() {
		return fid;
	}
	public void setFid(String fid) {
		this.fid = fid;
	}
	public ArrayList<FSPAmount> getOrder_detail() {
		return order_detail;
	}
	public void setOrder_detail(ArrayList<FSPAmount> order_detail) {
		this.order_detail = order_detail;
	}
	public String getDelivery_note() {
		return delivery_note;
	}
	public void setDelivery_note(String delivery_note) {
		this.delivery_note = delivery_note;
	}
	public Order(){
		System.out.println("In Order Constructor..");
	}	
	public boolean matchesOid(String oid){		
		return(this.oid.equals(oid));
	}
	public boolean matchesCid(String cid){		
		return(this.cid.equals(cid));
	}
	public boolean matchesFid(String fid){
		return(this.fid.equals(fid));
	}
}
