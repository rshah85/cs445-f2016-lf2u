package POJO;

public class Revenue_Month {
	private String mrid;
	private String name;
	private String start_date;
	private String end_date;
	private int orders_placed;
	private int orders_delivered;
	private int orders_open;
	private int orders_cancelled;
	private double total_revenue;
	private double total_products_revenue;
	private double total_delivery_revenue;
	private double total_lf2u_fees;
}
