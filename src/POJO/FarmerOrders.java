package POJO;

import java.util.List;

public class FarmerOrders {
	private String oid;
	private double product_total;
	private double delivery_charge;
	private double order_total;
	private String status;
	private String order_date;
	private String planned_delivery_date;
	private String actual_delivery_date;
	private Ordered_By orderby;
	private String delivery_address;
	private String note;
	private List<order_detail> order_detail;
	
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	
	public double getProduct_total() {
		return product_total;
	}
	public void setProduct_total(double product_total) {
		this.product_total = product_total;
	}
	public double getDelivery_charge() {
		return delivery_charge;
	}
	public void setDelivery_charge(double delivery_charge) {
		this.delivery_charge = delivery_charge;
	}
	public double getOrder_total() {
		return order_total;
	}
	public void setOrder_total(double order_total) {
		this.order_total = order_total;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getOrder_date() {
		return order_date;
	}
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
	public String getPlanned_delivery_date() {
		return planned_delivery_date;
	}
	public void setPlanned_delivery_date(String planned_delivery_date) {
		this.planned_delivery_date = planned_delivery_date;
	}
	public String getActual_delivery_date() {
		return actual_delivery_date;
	}
	public void setActual_delivery_date(String actual_delivery_date) {
		this.actual_delivery_date = actual_delivery_date;
	}
	public Ordered_By getOrderby() {
		return orderby;
	}
	public void setOrderby(Ordered_By orderby) {
		this.orderby = orderby;
	}
	public String getDelivery_address() {
		return delivery_address;
	}
	public void setDelivery_address(String delivery_address) {
		this.delivery_address = delivery_address;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public List<order_detail> getOrder_detail() {
		return order_detail;
	}
	public void setOrder_detail(List<order_detail> order_detail) {
		this.order_detail = order_detail;
	}		
}
