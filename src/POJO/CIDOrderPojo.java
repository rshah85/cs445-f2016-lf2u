package POJO;

public class CIDOrderPojo {
	private String cid;
	private Order order;
	private String order_date;
			
	public String getOrder_date() {
		return order_date;
	}
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}			
	public boolean matchesCid(String cid){
		return (this.cid.equals(cid));
	}
}
