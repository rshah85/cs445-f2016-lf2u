package POJO;

public class FarmerWithDeliveryCharge {
	private String fid;
	private double delivery_charge;
	public String getFid() {
		return fid;
	}
	public void setFid(String fid) {
		this.fid = fid;
	}	
	public double getDelivery_charge() {
		return delivery_charge;
	}
	public void setDelivery_charge(double delivery_charge) {
		this.delivery_charge = delivery_charge;
	}
	public boolean matchesFid(String fid){
		return(this.fid.equals(fid));
	}
}
