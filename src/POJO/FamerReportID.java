package POJO;

import java.util.List;

public class FamerReportID {
	private String frid;
	private String name;
	private List<FarmerOrders> orders;
	
	public String getFrid() {
		return frid;
	}
	public void setFrid(String frid) {
		this.frid = frid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FarmerOrders> getOrders() {
		return orders;
	}
	public void setOrders(List<FarmerOrders> orders) {
		this.orders = orders;
	}		
}
