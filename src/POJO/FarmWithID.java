package POJO;

public class FarmWithID {
	private String fid;
	private Farmer farmer;
	public String getFid() {
		return fid;
	}
	public void setFid(String fid) {
		this.fid = fid;
	}
	public Farmer getFarmer() {
		return farmer;
	}
	public void setFarmer(Farmer farmer) {
		this.farmer = farmer;
	}
	


	public boolean matchesFid(String fid){
		return (this.fid.equals(fid));
	}
}
