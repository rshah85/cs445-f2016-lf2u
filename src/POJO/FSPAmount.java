package POJO;

public class FSPAmount {
	private String fspid;
	private int amount;
	private String name;
	private String price;
	private double line_amount_total;
	private String oid;
			
	
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public double getLine_amount_total() {
		return line_amount_total;
	}
	public void setLine_amount_total(double line_amount_total) {
		this.line_amount_total = line_amount_total;
	}
	public String getFspid() {
		return fspid;
	}
	public void setFspid(String fspid) {
		this.fspid = fspid;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}		
	public boolean matchesOid(String oid){
		return(this.oid.equals(oid));
	}
}
