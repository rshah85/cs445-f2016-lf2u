package POJO;

public class Product {
	public String gcpid;
	public String note;
	public String start_date;
	public String end_date;
	public double price;
	public String product_unit;
	public String image;
	public String fid;
	public String fspid;	
	public String name;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFspid() {
		return fspid;
	}
	public void setFspid(String fspid) {
		this.fspid = fspid;
	}
	public String getGcpid() {
		return gcpid;
	}
	public void setGcpid(String gcpid) {
		this.gcpid = gcpid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getProduct_unit() {
		return product_unit;
	}
	public void setProduct_unit(String product_unit) {
		this.product_unit = product_unit;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getFid() {
		return fid;
	}
	public void setFid(String fid) {
		this.fid = fid;
	}	
	
	public boolean matchesfid(String fid){
		return (this.fid.equals(fid));
	}
	public boolean matchesGcpid(String gcpid){
		return (this.gcpid.equals(gcpid));
	}
}
