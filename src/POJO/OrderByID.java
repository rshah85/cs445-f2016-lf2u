package POJO;

import java.util.List;

public class OrderByID {
	private String oid;
	private String order_date;
	private String planned_delivery_date;
	private String actual_delivery_date;
	private String status;
	private farm_info farm_info;
	private List<order_detail> order_detail;
	private String delivery_note;
	private double products_total;
	private double delivery_charge;
	private double order_total;
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	public String getOrder_date() {
		return order_date;
	}
	public void setOrder_date(String order_date) {
		this.order_date = order_date;
	}
	public String getPlanned_delivery_date() {
		return planned_delivery_date;
	}
	public void setPlanned_delivery_date(String planned_delivery_date) {
		this.planned_delivery_date = planned_delivery_date;
	}
	public String getActual_delivery_date() {
		return actual_delivery_date;
	}
	public void setActual_delivery_date(String actual_delivery_date) {
		this.actual_delivery_date = actual_delivery_date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public farm_info getFarminfo() {
		return farm_info;
	}
	public void setFarminfo(farm_info farminfo) {
		this.farm_info = farminfo;
	}
	public List<order_detail> getOrderdetail() {
		return order_detail;
	}
	public void setOrderdetail(List<order_detail> orderdetail) {
		this.order_detail = orderdetail;
	}
	public String getDelivery_note() {
		return delivery_note;
	}
	public void setDelivery_note(String delivery_note) {
		this.delivery_note = delivery_note;
	}
	public double getProducts_total() {
		return products_total;
	}
	public void setProducts_total(double products_total) {
		this.products_total = products_total;
	}
	public double getDelivery_charge() {
		return delivery_charge;
	}
	public void setDelivery_charge(double delivery_charge) {
		this.delivery_charge = delivery_charge;
	}
	public double getOrder_total() {
		return order_total;
	}
	public void setOrder_total(double order_total) {
		this.order_total = order_total;
	}
}
