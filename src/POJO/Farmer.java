package POJO;

import java.util.ArrayList;


public class Farmer {
	private farm_info farm_info;
	private personal_info personal_info;
	private ArrayList<String> delivers_to;	
	
	public boolean isNil() {
        return false;
    }
	public Farmer() {
		System.out.println("In farmer....");
	}

	public farm_info getFarm_info() {
		return farm_info;
	}

	public void setFarm_info(farm_info farminfo) {
		this.farm_info = farminfo;
	}

	public personal_info getPersonal_info() {
		return personal_info;
	}

	public void setPersonal_info(personal_info personalinfo) {
		this.personal_info = personalinfo;
	}

	public ArrayList<String> getDelivers_to() {
		return delivers_to;
	}

	public void setDelivers_to(ArrayList<String> delivers_to) {
		this.delivers_to = delivers_to;
	}	
	
	public boolean matchesByZip(String zip){
		boolean found = false;
		for(int i=0;i<delivers_to.size();i++){
			if(delivers_to.get(i).equals(zip)){
				found = true;
			}
		}		
		return found;
	}	
}
