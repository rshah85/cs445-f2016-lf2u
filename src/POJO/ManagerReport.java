package POJO;

public class ManagerReport {
	public String mrid;
	public String name;
	public String getMrid() {
		return mrid;
	}
	public void setMrid(String mrid) {
		this.mrid = mrid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean matchesMrid(String mrid){
		return(this.mrid.equals(mrid));
	}
}
