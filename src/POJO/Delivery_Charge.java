package POJO;

public class Delivery_Charge {
	private double delivery_charge;

	public double getDelivery_charge() {
		return delivery_charge;
	}

	public void setDelivery_charge(double delivery_charge) {
		this.delivery_charge = delivery_charge;
	}
}
