package POJO;

public class Consumer {	

	private String cid;	
	private String name;
	private String street;
	private String zip;
	private String phone;
	private String email;

	
	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public Consumer(){
		System.out.println("In Customer....");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}		
	
	public boolean matchesCid(String cid){
		return (this.cid.equals(cid));
	}
	
    public boolean isNil() {
        return false;
    }
}
