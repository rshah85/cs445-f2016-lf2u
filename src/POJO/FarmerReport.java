package POJO;

public class FarmerReport {
	public String frid;
	public String name;
	public String getFrid() {
		return frid;
	}
	public void setFrid(String frid) {
		this.frid = frid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}		
	public boolean matchesFrid(String frid){
		return(this.frid.equals(frid));
	}
}
